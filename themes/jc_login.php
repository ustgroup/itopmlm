<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	
    <title><?= $title ?></title>
  
	<!-- Latest compiled and minified CSS -->
    <link href="<?=base_url()?>themes/jc_themes/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    
    <link rel="stylesheet" href="<?=base_url()?>themes/jc_themes/bootstrap/css/bootstrap-responsive.css">
    
    <script src="<?=base_url()?>themes/jc_themes/jquery-ui/jquery-1.9.1.js"></script>
    
    <link rel="stylesheet" href="<?=base_url()?>themes/jc_themes/jquery-ui/jquery-ui.css">
    
    <link href="<?=base_url()?>themes/jc_themes/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	<script src="<?=base_url()?>themes/jc_themes/jquery-ui/jquery-ui.js"></script>
    
    <link href="<?=base_url()?>themes/jc_themes/bootstrap/css/main.css" rel="stylesheet" type="text/css">
    
    <link href="<?=base_url()?>themes/jc_themes/bootstrap/css/new.css" rel="stylesheet" type="text/css">
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?=base_url()?>themes/jc_themes/bootstrap/js/bootstrap.js"></script>
    <!-- HTML5 shim for IE backwards compatibility -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if IE]>
    <link rel='stylesheet' type='text/css' href='<?=base_url()?>themes/jc_themes/ie.css' />
    <![endif]-->
   
  
   
	<?= $_scripts ?>
	<?= $_styles ?>
    
    <link rel="icon" type="image/ico" href="<?=base_url()?>themes/jc_themes/bootstrap/img/favicon.ico"/>
  	
</head>

<body style="background-image:url('<?=base_url()?>themes/jc_themes/images/new-login-bg-image.jpg');background-repeat:no-repeat;background-size:contain;background-position:center;">



<!-- <div class="container-fluid"> -->
<!--<div class="row-fluid">
<!-- <div class="container"> 
    	<div class="new-row-fluid">
    			<img class="img-responsive" width="100%" src="<?=base_url()?>images/banner-top.jpg">
            	<div class="span12 new-header-top"></div>
     </div>
        
        
        
    <!-- </div> 
</div>-->
<!-- </div> -->
<!--END HEAD CONTAINER-->

<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
	<?= $content ?>
	

	<!----STICKY FOOTER---->
	
	<!-- <div class="container-fluid"> -->
	<!--<div class="row-fluid">
	<!-- <div class="container"> 
	    	<div class="new-row-fluid">         	
	            	<div class="span12 login-sticky">
	            		<div class="container-fluid">
	            			<div class="span12 new-footer text-center">
	                              <p><?= $footer ?></p>
	                        </div>
	            		</div>
	            	</div>
	        </div>
	    <!-- </div> 
	</div>
	<!-- </div> -->
        <!----STICKY FOOTER---->
</div>
</div>
<!--END HEAD CONTAINER-->


   
</body>
</html>