<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	
    <title><?= $title ?></title>
    <!-- Latest compiled and minified CSS -->
   <link href="<?=base_url()?>themes/jc_themes/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    
    <link rel="stylesheet" href="<?=base_url()?>themes/jc_themes/bootstrap/css/bootstrap-responsive.css">
    
    <script src="<?=base_url()?>themes/jc_themes/jquery-ui/jquery-1.9.1.js"></script>
    
    <link rel="stylesheet" href="<?=base_url()?>themes/jc_themes/jquery-ui/jquery-ui.css">
    
    <link href="<?=base_url()?>themes/jc_themes/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	<script src="<?=base_url()?>themes/jc_themes/jquery-ui/jquery-ui.js"></script>
    
    <link href="<?=base_url()?>themes/jc_themes/bootstrap/css/main.css" rel="stylesheet" type="text/css">
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?=base_url()?>themes/jc_themes/bootstrap/js/bootstrap.js"></script>
	
   
   
    <?php /*
	<!-- Latest compiled and minified CSS -->
   
	<link href="<?=base_url()?>assets/pc_themes/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
	<script src="<?=base_url()?>assets/pc_themes/datatables/media/js/jquery.dataTables.js"></script>
	
   <?php
   	if($this->session->userdata('group')=='A'){
		?><link href="<?=base_url()?>assets/pc_themes/admin.css" rel="stylesheet" type="text/css"><?php
	}
   ?>
   */
   ?>
    <!-- HTML5 shim for IE backwards compatibility -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if IE]>
    <link rel='stylesheet' type='text/css' href='<?=base_url()?>themes/jc_themes/ie.css' />
    <![endif]-->
	<?= $_scripts ?>
	<?= $_styles ?>
    
    <style type="text/css">
		.dropdown-submenu > .dropdown-menu {
			top: 100%;
			left: 0;
			margin-top: 0;
			margin-left: -1px;
			-webkit-border-radius: 0 6px 6px 6px;
			-moz-border-radius: 0 6px 6px 6px;
			border-radius: 0 6px 6px 6px;
			
			}
			.dropdown-submenu > .dropdown-menu a:hover {background:#009900; }
			.dropdown-submenu > a:after {
				  content:none;
			}
			
			
			
			.dropdown-submenu > .dropdown-menu .dropdown-menu {
			
			top: 0;
			  left: 100%;
			  margin-top: -6px;
			  margin-left: -1px;
			  -webkit-border-radius: 0 6px 6px 6px;
			  -moz-border-radius: 0 6px 6px 6px;
			  border-radius: 0 6px 6px 6px;
			
			}
			.dropdown-menu .dropdown-submenu > a:after {
				content: " ";
			}
			
			.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .dropdown-submenu:hover > a, .dropdown-submenu:focus > a{
				background:#009900; 
			}
			.dash-title{
				font-size:14px; font-weight:bold;
				font-family:Verdana, Geneva, sans-serif;
			}
	</style>
		<style type="text/css">
		.form-horizontal .control-label{ width:180px;}
		.form-horizontal .controls { margin-left: 190px; }
		.ui-dialog, label, input, button, select, textarea {
			font-size: 12px;
			font-weight: normal;
			line-height: 12px;
		}
		.form-horizontal .control-group {
			margin-bottom: 5px;
		}
		input[type="text"]{
			padding:2px 6px;
		}
		.ui-dialog .ui-dialog-buttonpane button {
			padding:5px 8px;
		}
		.ui-dialog-titlebar-close{
			display:none;
		}
	</style>
	
  	<link rel="icon" type="image/ico" href="<?=base_url()?>themes/jc_themes/bootstrap/img/favicon.ico"/>
</head>

<body>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12 header-top">
			<div class="container">
				<div class="row-fluid">
                    <div class="span3">
                   		<img src="<?=base_url()?>themes/jc_themes/bootstrap/img/logo.png" style="margin: 3px 0px;">
                    </div>

                    <div class="span9">
                    	<?= $navmenu ?>
                    </div>
				</div>
			</div>
        </div>
	</div>
</div>

<!--END HEAD CONTAINER-->

<div class="container-fluid">
    <div class="row-fluid">
    	<div class="span12">
            <div class="container">  
			    <div class="row-fluid main">
  					 <div class="span12 box-content">
    					<div class="row-fluid">
    						<div class="span12 greenback">
    							<h3 class="dash-title" ><?= $header ?></h3>
                            </div>
                        </div>


						<?php $wide_col = (empty($sidebar))? 12 : 9 ;  ?> 
                        <div class="row-fluid">
                       		<?php if(!empty($sidebar)){ ?>
                        	<div class="span3">
								<?= $sidebar ?>
	                        </div>
    	                    <?php } ?>
                        
                        	<div class="span<?php echo $wide_col; ?> content-wrapper">
                        		<?= $content ?>
                       		</div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
    
    
    <div class="row-fluid"><div class="span12"></div></div>
    
    
    <!----STICKY FOOTER---->
    <div class="row-fluid">
   		<div class="span12 sticky">
    		<div class="container">
    			<div class="row-fluid">
    				<div class="span12 footer">
    					<?= $footer ?>
    				</div>
    			</div>
    		</div>
		</div>
	</div>
	<!----STICKY FOOTER---->

</div>
<!--END HEAD CONTAINER-->


 <div id="xmyModal" title="Change Password" style=" display:none" >
                 
                    <div class="modal-body" id="xfrmDiv">
                    	<form class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" >Old Password</label>
                                <div class="controls">
                                	*<input type="text" id="oldpw" value="" >
                             	</div>
                            </div>
                            <?php
								
							?>
                            <div class="control-group">
                                <label class="control-label" >New Password</label>
                                <div class="controls">
                                	*<input type="password" id="newpw" value="" >
                             	</div>
                            </div>
                               <div class="control-group">
                                <label class="control-label" >Confirm New Password</label>
                                <div class="controls">
                                	*<input type="password" id="conpw" value="" >
                             	</div>
                            </div>
                              <div style="text-align:center; display:none" id="xmsgLoader">
                      
                       <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
                       <br />                      <strong>                            
                            Saving...
                        </strong>
                    </div>
                            
                        </form>
                    </div>
                    
            </div>
			

   
</body>
</html>