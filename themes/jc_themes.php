<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	
    <title><?= $title ?></title>
  
	<!-- Latest compiled and minified CSS -->
    <link href="<?=base_url()?>themes/jc_themes/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
    
    <link rel="stylesheet" href="<?=base_url()?>themes/jc_themes/bootstrap/css/bootstrap-responsive.css">
    
    
    <link rel="stylesheet" href="<?=base_url()?>themes/jc_themes/jquery-ui/jquery-ui.css">
    
    <link href="<?=base_url()?>themes/jc_themes/font-awesome/css/font-awesome.min.css" rel="stylesheet">

	<script src="<?=base_url()?>themes/jc_themes/jquery-ui/jquery-1.9.1.js"></script>
	<script src="<?=base_url()?>themes/jc_themes/jquery-ui/jquery-ui.js"></script>
    
    <link href="<?=base_url()?>themes/jc_themes/bootstrap/css/main.css" rel="stylesheet" type="text/css">
    
    <link href="<?=base_url()?>themes/jc_themes/bootstrap/css/new.css" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="<?=base_url()?>themes/jc_themes/bootstrap/js/bootstrap.js"></script>
   
    <!-- HTML5 shim for IE backwards compatibility -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if IE]>
    <link rel='stylesheet' type='text/css' href='<?=base_url()?>themes/jc_themes/ie.css' />
    <![endif]-->
   
  
   
	<?= $_scripts ?>
	<?= $_styles ?>
    
    
  	<link rel="icon" type="image/ico" href="<?=base_url()?>themes/jc_themes/images/favicon.ico"/>
	
	
	<style type="text/css">
		.form-horizontal .control-label{ width:180px;}
		.form-horizontal .controls { margin-left: 190px; }
		.ui-dialog, label, input, button, select, textarea {
			font-size: 12px;
			font-weight: normal;
			line-height: 12px;
		}
		.form-horizontal .control-group {
			margin-bottom: 5px;
		}
		input[type="text"]{
			padding:2px 6px;
		}
		.ui-dialog .ui-dialog-buttonpane button {
			padding:5px 8px;
		}
		.ui-dialog-titlebar-close{
			display:none;
		}
	</style>
		 
</head>

<body>



<!-- <div class="container-fluid"> -->
<div class="row-fluid">
<!-- <div class="container"> -->
    	<div class="new-row-fluid">
    			<img class="img-responsive" width="100%" src="<?=base_url()?>images/banner-top.jpg">
            	<img class="img-responsive" width="100%" src="<?=base_url()?>images/banner.jpg">
            	<div class="span12 new-header-top"></div>
        </div>
        
        
        
    <!-- </div> -->
</div>
<!-- </div> -->
<!--END HEAD CONTAINER-->

<div class="container-fluid">
<div class="row-fluid">
<div class="span12">
	<?php
		$wide_col = (empty($sidebar))? 12 : 9 ;
	?> 
	
	<div class="container">  
        <div class="row-fluid main">
        	<?php if(!empty($sidebar)){ ?>
            <div class="span3" style="padding: 10px">
            	<div class="sidebar">
                	<?= $sidebar ?>
                   
                </div>
            </div>
            <?php } ?>
            
            
            <div class="span<?php echo $wide_col; ?> box-content">
            	<?= $content ?>
                
        	</div><!--END Content-Box-->
    	</div><!--END MAIN CONTAINER-->



	<!--END HEAD CONTAINER-->
	</div>

	<!----STICKY FOOTER---->
	
	<!-- <div class="container-fluid"> -->
	<div class="row-fluid">
	<!-- <div class="container"> -->
	    	<div class="new-row-fluid">         	
	            	<div class="span12 new-sticky">
	            		<div class="container-fluid">
	            			<div class="span12 new-footer text-center">
	                              <p><?= $footer ?></p>
	                        </div>
	            		</div>
	            	</div>
	        </div>
	    <!-- </div> -->
	</div>
	<!-- </div> -->
        <!----STICKY FOOTER---->
</div>
</div>

	
            <div id="xmyModal" title="Change Password" style=" display:none" >
                   
                    <div class="modal-body" id="xfrmDiv">
                    	<form class="form-horizontal">
                            <div class="control-group">
                                <label class="control-label" >Old Password</label>
                                <div class="controls">
                                	*<input type="text" id="oldpw" value="" >
                             	</div>
                            </div>
                            <?php
								
							?>
                            <div class="control-group">
                                <label class="control-label" >New Password</label>
                                <div class="controls">
                                	*<input type="password" id="newpw" value="" >
                             	</div>
                            </div>
                               <div class="control-group">
                                <label class="control-label" >Confirm New Password</label>
                                <div class="controls">
                                	*<input type="password" id="conpw" value="" >
                             	</div>
                            </div>
                             <div style="text-align:center; display:none" id="xmsgLoader">
                      
                       <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
                       <br />                      <strong>                            
                            Saving...
                        </strong>
                    </div>
                            
                        </form>
                    </div>
                    
            </div>
			

<!--END HEAD CONTAINER-->


<?php /*

	<div class="container">
		<?= $header ?>
		<?php
            $wide_col = (empty($sidebar))? 12 : 9 ;
        ?> 
        <?php if(!empty($navmenu)){ ?>
        	<div class="row-fluid">
        		<?= $navmenu ?>
            </div>
        <?php } ?> 
        <div class="row-fluid">
            <?php if(!empty($sidebar)){ ?>
                <div class="span3">
                    <?= $sidebar ?>
                </div>
            <?php } ?>
            
            <div class="span<?php echo $wide_col; ?>">
                <?= $content ?>
            </div>
        </div>
        
        
        
        <div class="row-fluid">
            <div class="span12 sticky">
                <div class="container">
                    <div class="row-fluid">
                        <div class="span12 footer">
                            <?= $footer ?>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

    </div>
   <?php /*
   
   
   
   <div id="wrapper">
         <div id="header">
            <?= $header ?>
         </div>
         <div id="main">
            <div id="content">
               <h2><?= $title ?></h2>
               <div class="post" style="width:70%; float:left;">
                  <?= $content ?>
               </div>
            </div>
            <div id="sidebar" style="width:25%  float:left;">sdsd
               <?= $sidebar ?>
            </div>
         </div>
         <div id="footer">
            <?= $footer ?>
         </div>
      </div>
   
   */
   ?>
   
   
</body>
</html>