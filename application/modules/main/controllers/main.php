<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() {
		
		parent::__construct();  
		
		$this->load->library('form_validation');
	
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				$this->load->model('mainModel','model');
				$res['data'] = $this->model->getData($this->session->userdata('id'));
				
				
				$slot['slot'] = $this->model->getSlot($this->session->userdata('id'));
				
				//print_r($res['data']); 
				$this->template->add_js('themes/jc_themes/chosen/chosen.jquery.js');
				$this->template->add_css('themes/jc_themes/chosen/chosen.css');
				$this->template->write_view('sidebar', 'sidebar',$slot);
				$this->template->write_view('content', 'dashboard', $res);
				$this->template->render();
			} else {
				if($this->session->userdata('group')=='A' || $this->session->userdata('group')=='B' ){
					redirect('admin');
				}
			}
		} else { 
			$this->form_validation->set_rules('userid', 'User ID', 'required|xss_clean|callback_ifExist');
			$this->form_validation->set_rules('userPassword', 'Password', 'required|xss_clean');
			//$this->form_validation->set_message('Invalid Login');
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button>', '</div>');
			
			if ($this->form_validation->run() == FALSE){
				//$this->load->view('admin/login');
				$this->template->set_master_template('../../themes/jc_login');
				$this->template->write_view('content', 'main');
				$this->template->render();
			}else{
				redirect('');
			}
		
		}
		
	}
	

	function ifExist($str){
		$this->load->model('mainModel','main');
	   	$res = $this->main->check_exists($this->input->post('userid'),$this->input->post('userPassword'));
		
		
	  	if ($res == 2) {
			$this->form_validation->set_message('ifExist', 'Username and Password do not match or you do not have an account yet!');
			return FALSE;
	   	} elseif ($res == 3) {
		   $this->form_validation->set_message('ifExist', 'Account already logged in!');
			return FALSE;
		} else {
			return TRUE;
		}
   	}
	
	public function logout() { 
		//$this->load->model('admin/loginModel','login');
		//$this->login->logMeOut();		
		$this->session->sess_destroy();
		redirect('');
	}
	
	public function updatePW(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M' || $this->session->userdata('group')=='A' || $this->session->userdata('group')=='B' ){
				//print_r($this->input->post('id'));
				if($this->session->userdata('id') == $this->input->post('id')){
					$postData = $this->input->post();	
					
					if ( empty($postData['oldpw']) || empty($postData['newpw']) || empty($postData['conpw']) ){
							echo json_encode(array("error"=>1, "msg"=>"* Required Fields!" ));
					}else{
						if( $postData['newpw'] != $postData['conpw'] ){
							echo json_encode(array("error"=>1, "msg"=>"New password and Confirm password not equal!" ));
						}else{
							$this->load->model('mainModel','model');
							$res = $this->model->chkPW($postData['oldpw'],$this->session->userdata('id'));
							if($res > 0){
								$res = $this->model->updatePW($postData['newpw'],$this->session->userdata('id'));
								echo json_encode(array("error"=>0, "msg"=>"Password updated!" ));								
							} else {
								echo json_encode(array("error"=>1, "msg"=>"Invalid old password!" ));
							}
						}
					}
					/*
					$this->load->model('ewalletModel','model');
					$res = $this->model->saveEncashment($postData['amt']);
					if($res > 0){
						echo json_encode(array("error"=>1, "msg"=>"Insuficient Fund : invalid transaction!" ));
					} else {
						echo json_encode(array("error"=>0, "msg"=>"" ));
					}
					*/
					
				} else {
					echo json_encode(array("error"=>1, "msg"=>"Error invalid Log-in!" ));
				}
			}
		} else {
			die;
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */