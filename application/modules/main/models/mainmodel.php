<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mainModel extends CI_Model {

		function __construct() {
			
			parent::__construct();
			$this->load->database();
		}	
		
		public function check_exists($userid, $password)
		{
			// md5($password)
			$query = $this->db->query("SELECT u.*,m.fname,m.lname FROM rbs_users AS u LEFT JOIN rbs_muster_table m ON m.userid=u.id WHERE u.username='".$userid."' AND u.password='".$password."' ");
			if ($query->num_rows() > 0)	{
				$row = $query->row();
				if($row->user_group == 1){
					$group = 'A';
				}elseif($row->user_group == 2){
					$group = 'M';
				}elseif($row->user_group == 3){
					$group = 'B';
				}
				$login = array(
							'id'=>$row->id, 
							'username'=>$row->username, 
							'name'=>$row->fname.' '.$row->lname, 
							'usertype'=> $group, 
							'userlevel'=>'1', 
							'email'=>$row->email
						);
				//print_r($login);
				//$login = array('id'=>100, 'name'=>'administrator', 'usertype'=>'M', 'userlevel'=>'1', 'email'=>'admin@localhost.com');
				//print_r($login); die;
				$login = (object) $login;
				$this->set_session($login);
				
				return 1;
			} 
			return 2;
			//die( 'ddd' );
			/*
			$this->db->select(" id, username, ");
			$this->db->select("CONCAT(f_name,' ', l_name) AS name ", FALSE);
			$this->db->from	("tbl_mlm_admin AS a");
			$this->db->where("a.IDNO ", $userid );
			$this->db->where("a.PASSWORD", md5($password) );			
			$login = $this->db->get()->result();	
			
			if ( is_array($login) && count($login) == 1 ) {
				//$this->db->select();
				$result = $this->db->query("SELECT user_id FROM tbl_mlm_sessions WHERE user_id='".$login[0]->ADMIN_ID_PK."' ");
				if ($result->num_rows() > 0) {
					return 3;
				} else {
					// Call set_session to set the user's session vars via CodeIgniter
					$this->set_session($login[0]);
					return 1;
				}
			}else{
				
				return 2;
			}
			
			if($userid == 'admin' && $password == '123456'){
				$login = array('id'=>100, 'name'=>'administrator', 'usertype'=>'M', 'userlevel'=>'1', 'email'=>'admin@localhost.com');
				$login = (object) $login;
				$this->set_session($login);
				return 1;
			} else {
				return 2;
			}
			*/
		}
		
		function set_session($item = array()) {
			$this->session->set_userdata( array(
					'id'=>$item->id,
					'username'=>$item->username, 
					'name'=>$item->name,
					'email'=>$item->email,
					'group'=>$item->usertype,
					'level'=>$item->userlevel,
					'isLoggedIn'=>true
				)
			);
			
			//return $this->set_session_user_id($this->session->userdata('session_id'), array('user_id' =>$item->ADMIN_ID_PK ));
			
		}
		
		function set_session_user_id($sessId='',$data=array()){
			$this->db->where('session_id', $sessId);
			return $this->db->update('tbl_mlm_sessions', $data); 
		}
		
		
		public function autoSessionLogout(){
			$timeout = $this->config->item('sess_expiration') + 600;
			$data = array(
			   'LOGIN' => 0
			);
			
			$current_time =  time() - $timeout;
			//echo time().' ==== '.$current_time;
			return $this->db->delete('tbl_mlm_sessions', array('last_activity < ' => $current_time));
			
		}
		
		public function logMeOut(){
			return $this->db->delete('tbl_mlm_sessions', array('user_id' => $this->session->userdata('id')));
			
		}
		
		public function chkPW($oldpw='',$id=0){
			
			$querys="SELECT id FROM rbs_users WHERE id = '".$id."' AND password = '".$oldpw."' ";
			$query = $this->db->query($querys);	
			return $query->num_rows() ;	
		}
		
		public function updatePW($newpw='',$id=0){
			$result = $this->db->query(" UPDATE rbs_users SET `password` = '".$newpw."'  WHERE id = '".$id."'  "); 							
			return true ;	
		}
		
		public function getSlot($userid=0){
			$querys="SELECT s.slot,m.fslot FROM rbs_muster_table  AS m
						LEFT JOIN rbs_slot AS s ON s.id=m.fslot
					 WHERE m.userid = '".$userid."' ";
			$query = $this->db->query($querys);	
			return $query->row();
			
		}
		
		public function getData($userid=0){
			
			
			$querys="SELECT total_coml,total_comr,total_comu FROM rbs_muster_table 
					 WHERE userid = '".$userid."' ";
			$query = $this->db->query($querys);	
			$res = $query->row();
			
			/*
			
			$querys="SELECT total_encashed FROM rbs_users 
					 WHERE id = '".$userid."' ";
			$query = $this->db->query($querys);	
			$encash = $query->row();
			
			$querys="SELECT SUM(amount) total_available FROM rbs_commision 
					 WHERE recipient_id = '".$userid."' AND available = 1 ";
			$query = $this->db->query($querys);	
			$totAva = $query->row();
			
			
			$querys="SELECT SUM(amount) total_amt FROM rbs_commision 
					 WHERE recipient_id = '".$userid."' ";
			$query = $this->db->query($querys);	
			$amt = $query->row();
			*/
			$arrayData = array("leadership" => $res->total_coml
								,"room" => $res->total_comr 
								, "unilevel" => $res->total_comu 
								
								); 
			// print_r($arrayData);
			return (object) $arrayData;	
			
		}
		
		

	
}