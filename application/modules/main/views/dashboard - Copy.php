<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
            <div class="row-fluid">
                	<div class="span12 greenback">
                    	<h3 class="dash-title">Dashboard</h3>
                    </div>
                </div>
                <!--END Content Box HEADER-->
            
            
                <div class="row-fluid">
                    <div class="span12 content-wrapper">
                        
                        <div class="row-fluid">
                            <div class="span7">
                                <div class="row-fluid">
                                	<div class="span12">
                                        <div class="btn-toolbar" style="text-align:right;">
                                            <div class="btn-group" >
                                                <a class="btn btn-success btn-small" href="javascript:void(0); loadInbox();"><i class="icon-envelope icon-white"></i> Inbox</a>
                                        		<a class="btn btn-success btn-small" href="javascript:void(0); loadSenItem();"><i class="icon-share-alt icon-white"></i> Sent</a>
                                                <a class="btn btn-success btn-small" href="#myModalCompose"  role="button" data-toggle="modal"><i class="icon-edit icon-white"></i> Compose</a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                                
                                <div class="row-fluid">
                                	<div class="span12" style="font-size:11px; box-shadow: 4px 4px 3px 0 #e0e0e0;">
                                    	<table class="table table-striped table-condensed table-hover" style="margin-bottom: 0px;">
                                            <thead>
                                                <tr>
                                                    <th colspan="2" class="btn-danger" id="msgHeader">Inbox</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    	<div style="max-height:350px; overflow:auto;">
                                        	<div style="text-align:center;" id="msgLoader">
                                            	<img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
                                            </div>
                                            <table class="table table-striped table-condensed table-hover">
                                                <tbody id="msgData">
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                
                                <!--h1>Test Header1</h1>
                                <h2>Test Header2</h2>
                                <h3>Test Header3</h3>
                                <h4>Test Header4</h4>
                                <h5>Test Header5</h5>
                                <h6>Test Header6</h6-->
                            </div>
                            
                            <div class="span5">
                                <div class="boxes box-green">
                                    <h1>Php <?php echo number_format($data->amt,2); ?></h1>
                                    <div class="box-title">Total Commision Earned</div>
                                </div>
                                <div class="boxes box-yellow">
                                	<?php $remainingBalance = $data->availabled - $data->encash; ?>
                                    <h1>Php <?php echo number_format($remainingBalance,2); ?></h1>
                                    <div class="box-title">Current Available Commision</div>
                                </div>
                                <div class="boxes box-red">
                                    <h1><?php echo $data->dr; ?></h1>
                                    <div class="box-title">Total Direct Affiliates</div>
                                </div>
                                
                            </div>
                        </div>
                            
                    </div>        
                </div>
                
                
                <div class="row-fluid"><div class="span12"></div></div>
                
               <div id="myModalCompose" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 id="myModalLabel" >Compose</h3>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal">
                                <div class="control-group">
                                    <label class="control-label" style="width:10%;" >From</label>
                                    <div class="controls" style="margin-left:15%;">
                                        <select class="my_select_box" id="recipient" data-placeholder="Select Your Options">
                                            <option value=""></option>
                                            <?php 
											$lvls = 1;
											$ctf =1;
											$myDown='';
											$alternate = 1;	
											$uLvl = 1;
											$tierString ='';
											foreach($genLists as $listsx){
												switch($uLvl){
													case 1 : $tierString ='1st Tier'; break;
													case 2 : $tierString ='2nd Tier'; break;
													case 3 : $tierString ='3rd Tier'; break;
													case 4 : $tierString ='4th Tier'; break;
													case 5 : $tierString ='5th Tier'; break;
													case 6 : $tierString ='6th Tier'; break;
													case 7 : $tierString ='7th Tier'; break;
													case 8 : $tierString ='8th Tier'; break;
													case 9 : $tierString ='9th Tier'; break;
													case 10 : $tierString ='10th Tier'; break;
													case 11 : $tierString ='11th Tier'; break;
													case 12 : $tierString ='12th Tier'; break;
													case 13 : $tierString ='13th Tier'; break;
													case 14 : $tierString ='14th Tier'; break;
													case 15 : $tierString ='15th Tier'; break;
												}
											?>
                                            <optgroup label="<?php echo $tierString; ?>">
                                            	<?php 
												$ctf =1;
												foreach($listsx as $listx){
													?>
                                                    <option value="<?php  echo $listx['id']; ?>"><?php  echo $listx['underName']; ?></option>
                                                    <?php
													
												}
												?>
                                             </optgroup>
                                          <?php 
										  	$uLvl++;
											} ?>
                                          </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" style="width:10%;" >Subject</label>
                                    <div class="controls" style="margin-left:15%;">
                                       <input type="text" id="subject" style="width:92%;" />
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" style="width:10%;" >Message</label>
                                    <div class="controls" style="margin-left:15%;">
                                       <textarea id="message" style="width:92%;"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        <button class="btn btn-success" onclick="sendToSender()">Send</button>
                    </div>
                </div> 
                
	<script type="text/javascript">
		$(document).ready(function(e) {
        	$(".my_select_box").chosen({allow_single_deselect:true, width:"95%"});
		
			$('#myModalCompose').on('hidden', function () {
				$("#recipient").val('');
				$("#subject").val('');
				$("#message").val('');
				$('.chosen-single').html('<span>Select Your Options</span><div><b></b></div>');
			})
			
			
			loadInbox();
			    
        });
		
		function loadInbox(){
			$("#msgHeader").html('Inbox');
			$("#msgLoader").css("display","");
			$.ajax({
				url : "<?=base_url()?>index.php/main/loadInbox",
				type: "POST",
				data : {},
				success:function(res, textStatus, jqXHR) 	{
					$("#msgLoader").css("display","none");
					$("#msgData").html(res);
				},
				error: function(jqXHR, textStatus, errorThrown)	{
					alert('error');   
				}
			}); 
		}
		
		function loadSenItem(){
			$("#msgHeader").html('Sent');
			$("#msgLoader").css("display","");
			$.ajax({
				url : "<?=base_url()?>index.php/main/loadSentItem",
				type: "POST",
				data : {},
				success:function(res, textStatus, jqXHR) 	{
					$("#msgLoader").css("display","none");
					$("#msgData").html(res);
				},
				error: function(jqXHR, textStatus, errorThrown)	{
					alert('error');   
				}
			}); 
		}
        
		function sendToSender(){
			$.ajax({
				url : "<?=base_url()?>index.php/main/sendMessage",
				type: "POST",
				data : {"recipient":$("#recipient").val(), "subject":$("#subject").val(), "message":$("#message").val()},
				success:function(res, textStatus, jqXHR) 	{
					//alert (res)
					var arObj = JSON.parse(res);
					if(parseInt(arObj.error) == 1){
						alert(arObj.msg);
					} else {
						alert('Message has been sent.');
						$('#myModalCompose').modal('hide');
					}
					//jsData = arObj.products;
					//filterProductLists('');
				},
				error: function(jqXHR, textStatus, errorThrown)	{
					alert('error');   
				}
			}); 
		}
    </script>