<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class affiliatesModel extends CI_Model {

		function __construct() {
			
			parent::__construct();
			$this->load->database();
		}
		
		public function _getDescendants( $id, $list = array(), $maxlevel = 15, $level = 1 )	{
			$query1 ="SELECT M.id,M.username AS underName,M.f_name,M.l_name,M.m_name,M.member_status,M.member_type,M.reg_date, S.username AS underSponsor 
							 FROM rbs_users AS M  
							 LEFT JOIN rbs_users AS S ON S.id = M.sponsor_id  
							 WHERE M.sponsor_id = ".$id." ";
			$query = $this->db->query($query1);
			if($query->num_rows() > 0 && $level <= $maxlevel){
				 foreach ($query->result_array() as $row){
					$id = $row['id'];
					$list[$level][]= $row;
					$list = $this->_getDescendants($id, $list, $maxlevel, $level + 1);
				}
			}
			return $list;
		}	
		
		
		public function getDownlines($userid) {
			$GenealogyLists = $this->_getDescendants($userid); 	
			return $GenealogyLists;
		}
		
		public function getDiretc($userid) {
			$GenealogyLists = $this->_getDescendants($userid); 	
			return $GenealogyLists;
		}
		
		public function IsDate( $Str ) {
		  $Stamp = strtotime( $Str );
		  $Month = date( 'm', $Stamp );
		  $Day   = date( 'd', $Stamp );
		  $Year  = date( 'Y', $Stamp );
		
		  return checkdate( $Month, $Day, $Year );
		}
		
		public function getCommision($userid, $fr, $to) {
			// $this->uri->segment(2)
			//print_r($this->uri->uri_to_assoc());
			
			
			$querys="SELECT c.*, CONCAT(s.f_name,' ',s.l_name) source_name, s.username FROM rbs_commision AS c  
					 LEFT JOIN rbs_users AS s ON s.id = c.source_id  
					 WHERE c.recipient_id = ".$userid." AND DATEDIFF(c.date_added,'" .$fr. "')>=0 AND  DATEDIFF('" .$to. "',c.date_added)>=0 ORDER BY c.date_added ASC ";
			$query = $this->db->query($querys);	
			return $query->result();			 
			//$rs1=mysql_query($query) or die(mysql_error().$query);
		}
	
}