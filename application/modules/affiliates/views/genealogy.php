<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

	 <div class="row-fluid">
            <div class="span12 greenback">
                <h3 class="dash-title">Affiliates</h3>
            </div>
        </div>
        <!--END Content Box HEADER-->
    
    	<?php // print_r($genLists); ?>
        
        <div class="row-fluid">
            <div class="span12 content-wrapper">
               
               
                <div class="accordion" id="accordion2">
                    <?php 
					$lvls = 1;
					$ctf =1;
					$myDown='';
					$alternate = 1;	
					$uLvl = 1;
					$tierString ='';
					foreach($genLists as $listsx){
						switch($uLvl){
							case 1 : $tierString ='1st Tier'; break;
							case 2 : $tierString ='2nd Tier'; break;
							case 3 : $tierString ='3rd Tier'; break;
							case 4 : $tierString ='4th Tier'; break;
							case 5 : $tierString ='5th Tier'; break;
							case 6 : $tierString ='6th Tier'; break;
							case 7 : $tierString ='7th Tier'; break;
							case 8 : $tierString ='8th Tier'; break;
							case 9 : $tierString ='9th Tier'; break;
							case 10 : $tierString ='10th Tier'; break;
							case 11 : $tierString ='11th Tier'; break;
							case 12 : $tierString ='12th Tier'; break;
							case 13 : $tierString ='13th Tier'; break;
							case 14 : $tierString ='14th Tier'; break;
							case 15 : $tierString ='15th Tier'; break;
						}
					?>
                    <div class="accordion-group">
                    	<div class="accordion-heading">
                   			<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $uLvl; ?>"><?php echo $tierString; ?></a>
	                    </div>
                        <div id="collapseOne<?php echo $uLvl; ?>" class="accordion-body collapse <?php echo ($uLvl==1)? 'in' : ''; ?>">
        	            	<div class="accordion-inner">
                   				<table class="table table-striped table-hover" style="border:#999999 solid 1px; font-size:12px;">
                                <thead>
                                    <tr height="30">
                                        <th style="padding-bottom:2px; padding-top:2px;">#</th>
                                        <th style="padding-bottom:2px; padding-top:2px;">Name</th>
                                        <th style="padding-bottom:2px; padding-top:2px;">Username</th>
                                        
                    
                                        <th style="padding-bottom:2px; padding-top:2px;">Sponsor</th>
                                        <th style="padding-bottom:2px; padding-top:2px;">Date Register</th>
                                    </tr>
                                </thead>
		                    	<tbody>
                        	<?php
							$ctf =1;
							foreach($listsx as $listx){
								
								?><tr>
									<td><?php echo $ctf; ?></td>
                                    <td><?php  echo $listx['f_name'].' '.$listx['l_name']; ?></td>
									<td><?php  echo $listx['underName']; ?></td>
                                    
									<td><?php  echo $listx['underSponsor']; ?></td>
									<td><?php echo date("M j, Y",strtotime($listx['reg_date']) ) ; ?></td>
								</tr><?php
								
								$ctf++; 
								$alternate = 1 - $alternate;
							}
							?>	
                            	</tbody>
                            </table>
                            </div>
        	            </div>
                    </div>
                            <?php
							$uLvl++;
					}
					?>
    	                
                    
                    
                    
                    
                </div>
                
                
                
            </div>        
        </div>
    