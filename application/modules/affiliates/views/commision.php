<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
	 
	 <style type="text/css">
	 	.ui-datepicker{
			font-size:12px;
		}
		.ui-datepicker select.ui-datepicker-month, .ui-datepicker select.ui-datepicker-year {
			height: 100%;
		}
		.ui-datepicker .ui-datepicker-prev span, .ui-datepicker .ui-datepicker-next span {
			
			margin-top: -3px;
		}
	 </style>
	 <script>
		$(function() {
			$( "#d_from" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd",
			numberOfMonths: 1,
			onClose: function( selectedDate ) {
			$( "#d_to" ).datepicker( "option", "minDate", selectedDate );
			}
			});
			$( "#d_to" ).datepicker({
			
			changeMonth: true,
			changeYear: true,
			dateFormat: "yy-mm-dd",
			numberOfMonths: 1,
			onClose: function( selectedDate ) {
			$( "#d_from" ).datepicker( "option", "maxDate", selectedDate );
			}
			});
		});
		
		function filterDate(){
			//alert($("#d_from").val());
			window.location = '<?=base_url()?>index.php/affiliates/commision/fr/'+$("#d_from").val()+'/to/'+$("#d_to").val();
		}
	</script>
	<div class="row-fluid">
		<div class="span12 greenback">
        	<h3 class="dash-title">Affiliates Commision</h3>
        </div>
    </div>
        <!--END Content Box HEADER-->
    	
    	<?php // print_r($genLists); 
		
		?>
        <div class="row-fluid">
            <div class="span12 content-wrapper">
            	<label for="d_from" style="float:left; margin:3px 5px 0 0">From</label>
                <input type="text" id="d_from" name="d_from" style="float:left; cursor:pointer;" readonly="readonly" value="<?php echo $fr; ?>">
                <label for="d_to"  style="float:left;margin:3px 5px;">to</label>
                <input type="text" id="d_to" name="d_to" style="float:left; cursor:pointer;" readonly="readonly"  value="<?php echo $to; ?>">
                <input class="btn btn-success" type="button" style="float:left;margin:0 5px;" value="Show" onclick="filterDate()">
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 content-wrapper">
                
                <table class="table table-striped table-hover" style="border:#999999 solid 1px; font-size:12px;">
					<thead>
                    	<tr height="30">
                            <th style="padding-bottom:2px; padding-top:2px;">#</th>
                            <th style="padding-bottom:2px; padding-top:2px;">Name</th>
                            <th style="padding-bottom:2px; padding-top:2px;">Username</th>
                            <th style="padding-bottom:2px; padding-top:2px;">Tier</th>
                            <th style="padding-bottom:2px; padding-top:2px;">Date</th>
                            
                            <th style="padding-bottom:2px; padding-top:2px;">Type</th>
                            <th style="padding-bottom:2px; padding-top:2px;">Status</th>
                            <th style="padding-bottom:2px; padding-top:2px;">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
						//print_r($items ); 
                       
							$ctf =1;
							foreach($items as $item){
								
								?><tr>
									<td><?php echo $ctf; ?></td>
                                    <td><?php  echo $item->source_name; ?></td>
									<td><?php  echo $item->username; ?></td>
                                    
									<td>
										<?php  //echo $item->source_level; 
											switch($item->source_level){
												case 1 : $tierString ='1st Tier'; break;
												case 2 : $tierString ='2nd Tier'; break;
												case 3 : $tierString ='3rd Tier'; break;
												case 4 : $tierString ='4th Tier'; break;
												case 5 : $tierString ='5th Tier'; break;
												case 6 : $tierString ='6th Tier'; break;
												case 7 : $tierString ='7th Tier'; break;
												case 8 : $tierString ='8th Tier'; break;
												case 9 : $tierString ='9th Tier'; break;
												case 10 : $tierString ='10th Tier'; break;
												case 11 : $tierString ='11th Tier'; break;
												case 12 : $tierString ='12th Tier'; break;
												case 13 : $tierString ='13th Tier'; break;
												case 14 : $tierString ='14th Tier'; break;
												case 15 : $tierString ='15th Tier'; break;
											}
											echo $tierString; 
                                        ?>
                                    </td>
									<td><?php echo date("M j, Y",strtotime($item->date_added) ) ; ?></td>
                                    <td><?php  
										//echo $item->payment_type; 
										if($item->payment_type == 1) {		echo 'Cash';
										}elseif($item->payment_type == 2) {	echo 'Dragonpay';
										}elseif($item->payment_type == 3) {	echo 'Credit Card';}
									?></td>
                                    <td><?php  echo ($item->available == 1)? 'Available' : 'Pending'; ?></td>
                                    <td><?php  echo number_format($item->amount,2); ?></td>
								</tr><?php
								
								$ctf++; 
								
							}
							
                        ?>
                    </tbody>	
                </table>
            </div>        
        </div>
    