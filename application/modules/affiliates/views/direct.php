<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

	 <div class="row-fluid">
            <div class="span12 greenback">
                <h3 class="dash-title">Direct Affiliates</h3>
            </div>
        </div>
        <!--END Content Box HEADER-->
    
    	<?php // print_r($genLists); ?>
        
        <div class="row-fluid">
            <div class="span12 content-wrapper">
                
                <table class="table table-striped table-hover" style="border:#999999 solid 1px; font-size:12px;">
					<thead>
                    	<tr height="30">
                            <th style="padding-bottom:2px; padding-top:2px;">#</th>
                            <th style="padding-bottom:2px; padding-top:2px;">Name</th>
                            <th style="padding-bottom:2px; padding-top:2px;">Username</th>
                            
        
                            <th style="padding-bottom:2px; padding-top:2px;">Sponsor</th>
                            <th style="padding-bottom:2px; padding-top:2px;">Date Register</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $lvls = 1;
                        $ctf =1;
                        $myDown='';
                        $alternate = 1;	
						$uLvl = 1;
						foreach($genLists as $listsx){
							//echo $uLvl;
							?>
							
							<?php
							$ctf =1;
							foreach($listsx as $listx){
								
								?><tr>
									<td><?php echo $ctf; ?></td>
                                    <td><?php  echo $listx['f_name'].' '.$listx['l_name']; ?></td>
									<td><?php  echo $listx['underName']; ?></td>
                                    
									<td><?php  echo $listx['underSponsor']; ?></td>
									<td><?php echo date("M j, Y",strtotime($listx['reg_date']) ) ; ?></td>
								</tr><?php
								
								$ctf++; 
								$alternate = 1 - $alternate;
							}
							$uLvl++;
						}	
                        ?>
                    </tbody>	
                </table>
            </div>        
        </div>
    