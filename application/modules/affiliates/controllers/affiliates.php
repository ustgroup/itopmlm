<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Affiliates extends CI_Controller {

	public function __construct() {
		
		parent::__construct();  
		
		$this->load->library('form_validation');
	
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()	{
		$this->genealogy();	
	}
	
	public function direct(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				$this->load->model('affiliatesModel','model');
				$res['genLists'] = $this->model->getDownlines($this->session->userdata('id'), 1);
				$this->template->write_view('sidebar', 'main/sidebar');
				$this->template->write_view('content', 'direct', $res);
				$this->template->render();
			} else {
				if($this->session->userdata('group')=='A'){
					redirect('admin');
				}
			}
		} else { 
			
				redirect('');
			
		
		}
	}
	
	public function genealogy(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				$this->load->model('affiliatesModel','model');
				$res['genLists'] = $this->model->getDownlines($this->session->userdata('id'));
				//print_r($this->session->userdata); 
				$this->template->write_view('sidebar', 'main/sidebar');
				$this->template->write_view('content', 'genealogy', $res);
				$this->template->render();
			} else {
				if($this->session->userdata('group')=='A'){
					redirect('admin');
				}
			}
		} else { 
				redirect('');
		}
	}
	
	
	public function commision(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				$this->load->model('affiliatesModel','model');
				
				$dateStr = $this->uri->uri_to_assoc();
				$res['fr'] = date("Y-m-d", strtotime("-5 day", strtotime(date("Y-m-d"))) );
				$res['to'] = date("Y-m-d");
				
				if(isset($dateStr['fr']) && isset($dateStr['to']) ){	
					if($this->model->IsDate($dateStr['fr']) && $this->model->IsDate($dateStr['to']) ){
						$res['fr'] = $dateStr['fr'];
						$res['to'] = $dateStr['to'];
					}
				}
				
				$res['items'] = $this->model->getCommision($this->session->userdata('id'),$res['fr'],$res['to']);
				//print_r($this->session->userdata); 
				$this->template->write_view('sidebar', 'main/sidebar');
				$this->template->write_view('content', 'commision', $res);
				$this->template->render();
			} else {
				if($this->session->userdata('group')=='A'){
					redirect('admin');
				}
			}
		} else { 
				redirect('');
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */