<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct() {
		
		parent::__construct();  
		
		$this->load->library('form_validation');
	
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('registerModel','model');
		
		$res['cityprov'] = $this->model->cityprov();
		$res['muni'] = $this->model->muni();
		$res['upline'] = $this->model->getUpline();
		//print_r($items);	
		
		$this->template->set_master_template('../../themes/jc_themes');
		$this->template->write_view('content', 'register',$res);
		$this->template->render();
		
	}
	
	  function addmember() {	   	
			$data = $this->input->post();	
			$this->load->model('registerModel','model');		
			
			$error = 0;				
			$mess = "";		
			//if (  empty($data['lname']) || empty($data['fname'])  || empty($data['usernm']) || empty($data['cityprov']) || empty($data['muni']) || empty($data['street']) || empty($data['birthdate'])  || empty($data['regcode'])  || empty($data['sponsor']) || empty($data['upline']) || empty($data['placement'])	)
			if (  empty($data['lname']) || empty($data['fname'])  || empty($data['usernm']) || empty($data['regcode'])  || empty($data['sponsor']) || empty($data['upline']) || empty($data['placement'])	)
				{						
					$mess  .= "* Required Fields! \n";						
				}else{
					
					//control username
					if (!preg_match('/^[a-zA-Z0-9_-]+$/',$data['usernm'])){
						$mess  .= "Invalid username input! \n";
					}else{
						//control length of character
						if(strlen($data['usernm']) < 3 || strlen($data['usernm']) > 25) { 					
							$mess  .= "User name - Minimum character 3!, Maximum character 25!  \n";
						}
						//control if user name already existed
						if($this->model->chkusername($data['usernm'])){
							$mess  .= "User name already used!  \n";
						}					
					}
					
					//control regcode
					$chkcode = $this->model->chkcode($data['regcode']);
					
					if (count($chkcode) >0){
						if($chkcode->userid >0){
								$mess  .= "Registration Code already used!  \n";	
						}else{
							//$data['slot'] = $chkcode->fslot;
							$data['paidorfree'] = $chkcode->paidorfree;
						}
					}else{
						$mess  .= "Invalid Registration Code!  \n";
					}
					
					//control Sponsor
					$chksponsor = $this->model->chksponsor($data['sponsor']);
					
					if($chksponsor == 0){
							$mess  .= "Sponsor not found! \n";
					}else{
						$data['sponsorid'] = $chksponsor;
					}
					
					//control Upline
					$chkupline = $this->model->chkupline($data['upline']);
					
					if($chkupline == 0){
							$mess  .= "Upline not found! \n";
					}else{
						$data['uplineid'] = $chkupline;
						
						//control Upline
						$chkplacement = $this->model->chkplacement($data['uplineid'],$data['placement']);
						
						if($chkplacement == 1){
								$mess  .= "Placement Not Available! \n";
						}
						
						
						
					}
					
					
					
			}
				
			if ($mess == "") {						
					$ret = $this->model->store($data);
					$mess = 'Records Save!';
			}else{
				$error = 1;	
			}
			
			$gg = array('error' => $error  ,  'mes' => $mess );	
			echo json_encode($gg);	
			die();
			
   }
	

	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */