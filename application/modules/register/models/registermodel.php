<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class registermodel extends CI_Model {

		function __construct() {
			
			parent::__construct();
			$this->load->database();
		}	
		
		
		public function cityprov(){
			$querys="SELECT * FROM rbs_cityprov order by cityprov asc ";
			//echo $querys;
			$query = $this->db->query($querys);	
			
			return $query->result() ;
		}
		public function muni(){
			$querys="SELECT * FROM rbs_muni order by muni asc ";
			//echo $querys;
			$query = $this->db->query($querys);	
			
			return $query->result() ;
		}
		public function getUpline(){
			$getData = $this->input->get();
			$querys="SELECT username FROM rbs_muster_table WHERE `userid` ='".$getData['topid']."'  ";
			//echo $querys;
			$query = $this->db->query($querys);				
			//print_r($query->row());
			return $query->row() ;
			
		}
		///////////////////////
		
		public function chkusername($record)
		{
		
			$result = $this->db->query("SELECT userid FROM rbs_muster_table	WHERE `username` ='".$record."'  ");						
			$res = $result->row();
		
			if (count($res) >0)
			{					
				return true;				
			}else{				
				return false;
			}
			
		}	
		public function chkcode($record)
		{		
			$result = $this->db->query("SELECT userid,fslot,paidorfree FROM rbs_regcodes	WHERE `fcode` ='".$record."'  ");									
			return $result->row();
			
		}	
		
		public function chksponsor($record)
		{		
			$result = $this->db->query("SELECT userid FROM rbs_muster_table	WHERE `username` ='".$record."'  ");						
			$res = $result->row();
		
			if (count($res) >0)
			{													
				return $res->userid;				
			}else{				
				return 0;
			}
			
		}	

		public function chkupline($record)
		{		
			$result = $this->db->query("SELECT userid FROM rbs_muster_table	WHERE `username` ='".$record."'  ");						
			$res = $result->row();
		
			if (count($res) >0)
			{													
				return $res->userid;				
			}else{				
				return 0;
			}
			
		}	
		
		public function chkplacement($record,$placement)
		{		
			$result = $this->db->query("SELECT `userid`,`left`,`right` FROM rbs_muster_table	WHERE `userid` ='".$record."'  ");						
			$res = $result->row();
		
			$ret = 0;
			if (count($res) >0)
			{													
				if($placement =='L'){
					if($res->left >0){
						$ret = 1;
					}
				}else{
					if($res->right >0){
						$ret = 1;
					}
				}
			}
			return 	$ret ;					
		}
		
		public function store($data)
		{
				//add user
				set_time_limit(0);	
				$cdate = date("Y-m-d H:i:s");
				$result = $this->db->query("INSERT INTO rbs_users(username,password,user_group) 	VALUES(	'".$data['usernm']."','123456','2')"); 			
				$newid = $this->db->insert_id();
				
				
				$dir=0;
				$pts=0;
				$maxpts=0;
					
				//add master table				
				/*
				$result = $this->db->query(" INSERT INTO `rbs_muster_table` (`fname`,`lname`,`mname`,`username`,`paidorfree`,`userid`,`sponsor`,`upline`,`cityprov`,`muni`,`street`,`gender`,`birthdate`,`mobile`,`regdate`,`upposition`,`maxpts`)
				*/
				$result = $this->db->query(" INSERT INTO `rbs_muster_table` (`fname`,`lname`,`mname`,`username`,`paidorfree`,`userid`,`sponsor`,`upline`,`gender`,`regdate`,`upposition`)
				VALUES (							
					'".str_replace("'","&rsquo;",strip_tags($data['fname']))."'
					,'".str_replace("'","&rsquo;",strip_tags($data['lname']))."'																				
					,'".str_replace("'","&rsquo;",strip_tags($data['mname']))."'		
					,'".str_replace("'","&rsquo;",strip_tags($data['usernm']))."'		
					,'".$data['paidorfree']."'	
					,'".$newid."'										
					,'".$data['sponsorid']."'										
					,'".$data['uplineid']."'															
					,'".str_replace("'","&rsquo;",strip_tags($data['gender']))."'									
					,'".$cdate."'						
					,'".$data['placement']."'																																						
					) "); 			
				//Update direct commission table	
				
				$result = $this->db->query("INSERT INTO rbs_table_list(userid,tablef) 	VALUES(	'".$newid."','1')"); 			
				
				//update placement
				if($data['placement'] =='L'){
					$result = $this->db->query(" UPDATE rbs_muster_table SET `left` = ".$newid." WHERE userid = '".$data['uplineid']."' "); 	
				}else{
					$result = $this->db->query(" UPDATE rbs_muster_table SET `right` = ".$newid." WHERE userid = '".$data['uplineid']."' "); 	
				}
				//update direct commission
				$result = $this->db->query(" UPDATE rbs_regcodes SET `userid` = '".$newid."', `dateencode` = '".$cdate."'  WHERE fcode = '".$data['regcode']."' "); 																
				$xms = $this->savebinarylevel($newid,$data['uplineid'],1,$data['placement'],$data['placement'],1,$pts);					
				if($data['paidorfree'] =='P'){
					$ms2 = $this->entryuni($newid, 1, $newid );				
				}
				$cur_date = date('Y-m-d');
				if($data['paidorfree'] =='P'){
					$result = $this->db->query(" SELECT 				              				
								 b.topid
								,b.position
								,b.userid	
								,l.leftf	
								,l.rightf
								FROM `rbs_binarylevel` AS b 
								LEFT JOIN `rbs_table_list` l ON l.userid=b.topid																							 
								WHERE b.`userid` = '".$newid."'  AND l.tablef = 1 AND l.stat =0  ");		
								
					$items = $result->result();
					if (count($items) >0){						
							foreach ($items as $itm){
								if($itm->position == 'L' && $itm->leftf < 7){
									$result = $this->db->query(" UPDATE rbs_table_list SET `leftf` = `leftf` + 1   WHERE userid = '".$itm->topid."' AND tablef = 1 "); 
									$result = $this->db->query("INSERT INTO rbs_table_downline(userid,tablef,topid,position) 	VALUES(	'".$newid."','1','".$itm->topid."' , '".$itm->position."' )"); 			
								}elseif($itm->position == 'R' && $itm->rightf < 7){
									$result = $this->db->query(" UPDATE rbs_table_list SET `rightf` = `rightf` + 1   WHERE userid = '".$itm->topid."' AND tablef = 1 "); 
									$result = $this->db->query("INSERT INTO rbs_table_downline(userid,tablef,topid,position) 	VALUES(	'".$newid."','1','".$itm->topid."' , '".$itm->position."' )"); 
								}
							}
					}
				}	
				
				$result = $this->db->query(" SELECT tablef			              											
							FROM `rbs_table_list` 									 
							ORDER BY tablef DESC LIMIT 1 ");					
				$items = $result->row();	
				$table = $items->tablef+1;
				/*
				$x = 0; 
				$ff = true;
				while($ff) {
					$x++;
				*/		
				for($x =1;$x<=$table;$x++) {
				
					$nextable=$x+1;															
					$result = $this->db->query(" SELECT userid,id,tablef	FROM `rbs_table_list`										 
							WHERE `tablef` = '".$x."' AND `leftf` = 7 AND  `rightf` =7 AND `stat` = 0 	ORDER BY id ASC ");										
					$items = $result->result();					
					if ( count($items) >0 ) {	
							
							foreach ($items as $itm){
								$result = $this->db->query(" UPDATE rbs_table_list SET `stat` =  1   WHERE id = '".$itm->id."' ");
								$result = $this->db->query(" UPDATE rbs_muster_table  SET `total_comr` = `total_comr` + 11200  where `userid` = '".$itm->userid."'   "); 	
								$result = $this->db->query("INSERT INTO rbs_commision (recipient_id,source_id,amount,date_added,com_type,tablef) 	
											VALUES(	'".$itm->userid."'
											,'0'
											,'11200'
											,'".$cur_date."'
											,'R'
											,'".$itm->tablef."'
											)"); 
								$result = $this->db->query("INSERT INTO rbs_table_list (userid,tablef)	VALUES(	'".$itm->userid."','".$nextable."'  )"); 			
								$newid = $itm->userid;
								////////////////////INSER NEW DOWNLINE///////////////////////
								
								$result = $this->db->query(" SELECT 				              				
								 b.topid
								,b.position
								,b.userid	
								,l.leftf	
								,l.rightf	
								FROM `rbs_binarylevel` AS b 
								LEFT JOIN `rbs_table_list` l ON l.userid=b.topid																							 				 
								WHERE b.`userid` = '".$newid."' AND l.tablef = '".$nextable."' AND l.stat =0  ");										
								
								$items = $result->result();
								if (count($items) >0){
										
										foreach ($items as $itm){
											if($itm->position == 'L' && $itm->leftf < 7){
												$result = $this->db->query(" UPDATE rbs_table_list SET `leftf` = `leftf` + 1   WHERE userid = '".$itm->topid."' AND tablef = '".$nextable."' "); 
												$result = $this->db->query("INSERT INTO rbs_table_downline(userid,tablef,topid,position) 	VALUES(	'".$newid."','".$nextable."','".$itm->topid."' , '".$itm->position."' )"); 			
											}elseif($itm->position == 'R' && $itm->rightf < 7){
												$result = $this->db->query(" UPDATE rbs_table_list SET `rightf` = `rightf` + 1   WHERE userid = '".$itm->topid."' AND tablef = '".$nextable."' "); 
												$result = $this->db->query("INSERT INTO rbs_table_downline(userid,tablef,topid,position) 	VALUES(	'".$newid."','".$nextable."','".$itm->topid."' , '".$itm->position."' )"); 
											}
										}
										
										
										
								}
								
								////////////////////END INSER NEW DOWNLINE///////////////////
								
							}
							
						}	
					/*
					if($x == $table ) {
						$ff=false;
					}
				 	*/
				}
				
				  

			return true;
		}
		
	public function entryuni($newsponsorid = null, $lev = 1,$newid = 0) 
	{
				
			$result = $this->db->query("SELECT `sponsor` FROM rbs_muster_table	WHERE `userid` ='".$newsponsorid."'  ");						
			$items = $result->row();
			$cur_date = date('Y-m-d');
				if (count($items) >0 && $lev <= 10 && $items->sponsor >0){ // found get sponsor id for the next computation				
				$sponsorid = $items->sponsor; //sponsor id
				
				
			
				$result = $this->db->query(" UPDATE rbs_muster_table  SET `total_coml` = `total_coml` + 10  where `userid` = '".$sponsorid."'   "); 	
				$result = $this->db->query("INSERT INTO rbs_commision (recipient_id,source_id,amount,date_added,com_type,leader_level) 	
											VALUES(	'".$sponsorid."'
											,'".$newid."'
											,'10'
											,'".$cur_date."'
											,'L'
											,'".$lev."'
											)"); 
															
					$mote = $this->entryuni($sponsorid, $lev + 1,$newid);
				}
				else {
				return 'ok';
					}
	}
		
		
	public function savebinarylevel($newid = null,$upline = null,$lev = null,$position = null,$lr = null,$ct = null,$pts=0 )
	{		
		$xc = 0;				
		$result = $this->db->query(" INSERT INTO `rbs_binarylevel`(`userid`,`topid`,`level`,`position`)
		VALUES ('".$newid."','".$upline."','".$lev."','".$position."')  "); 
			
			/*
			if($position=='L'){									
				$result = $this->db->query(" UPDATE rbs_muster_table  SET `lpoints` = `lpoints` + $pts  where `userid` = '".$upline."'   "); 	
			}else{
				$result = $this->db->query(" UPDATE rbs_muster_table  SET `rpoints` = `rpoints` + $pts  where `userid` = '".$upline."'   "); 					
			}		
			*/
		///////////////////////////////////////
		
		
		$nextupline = 0;		
		$result = $this->db->query("SELECT `upline` FROM rbs_muster_table	WHERE `userid` ='".$upline."'  ");						
		$items = $result->row();
		
			
		
		if (count($items) >0){ 
		$nextupline = $items->upline;
		
			  if ($nextupline == 0){
				return $nextupline;
				}
			  else{	
				
					$result = $this->db->query("SELECT `left` FROM rbs_muster_table	WHERE `userid` ='".$nextupline."'  ");						
					$items = $result->row();
				
					
					if (count($items) >0){// found 
						
						if ($items->left == $upline){
							$newposition = 'L';
						}
						else{
							$newposition = 'R';
						}
					
					}				
				$kamote = $this->savebinarylevel($newid,$nextupline,$lev+1,$newposition,$lr,$ct+$xc,$pts);						
				
			  }	
		}
	}
		
	
}