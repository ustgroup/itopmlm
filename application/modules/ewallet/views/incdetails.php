<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
	<!--style type="text/css">
		.form-horizontal .control-label{ width:180px;}
		.form-horizontal .controls { margin-left: 190px; }
		.ui-dialog, label, input, button, select, textarea {
			font-size: 12px;
			font-weight: normal;
			line-height: 12px;
		}
		.form-horizontal .control-group {
			margin-bottom: 5px;
		}
		input[type="text"]{
			padding:2px 6px;
		}
		.ui-dialog .ui-dialog-buttonpane button {
			padding:5px 8px;
		}
		.ui-dialog-titlebar-close{
			display:none;
		}
	</style-->
	<style type="text/css">
	.amt{
		text-align:right !important;
	}
	.type{
		text-align:center !important;
	}
	</style>
           
            <!-- <div class="row-fluid">
                <div class="span12 greenback">
                    <h3 class="dash-title">Income Details</h3>
                </div>
            </div> -->
            <!--END Content Box HEADER-->
        
        	<?php
				// print_r($data);
			?>    
                
         
            
            <div class="row-fluid">
                <div class="span12" style="padding:0px 10px;padding-bottom:20px">
					<br>
					 <table id="regcodesLists" class="table table-striped table-hover table-bordered" >
						<thead>
							<tr style="background:#FDFDFD;">
								<th style="width:15px">#</th>
								<th>Date</th>                
								<th style="text-align:right">Amount</th>
								<th style="text-align:right">Type</th>
							</tr>
						</thead>
						
					</table>
				<table>
					<tr>
					<td style="font-weight:bold">TOTAL AMOUNT : <?php echo number_format($items->total,2);  ?> </td>
					</tr>
				</table>
				<br clear="all">
					<?php /* ?>
                    <table class="table table-striped table-condensed table-hover">
                    	<thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>								
                                <th style="text-align:right">Amount</th>
								<th style="text-align:right">Type</th>
								
                                
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
							$ctr = 1;
							$varAmt = array();
							$varAmt['amount'] = 0;
							$type = '';
							foreach($items as $itm) { 
								
								if($itm->com_type =='I'){
									 $type = 'Leadership';
								}elseif($itm->com_type =='P'){
									 $type = 'Entry Pairing';
								}elseif($itm->com_type =='E'){
									 $type = 'Exit Table '.$itm->tablef;
								}	
								
									 
								?>
                                <tr>
                                    <td><?php echo $ctr; ?></td>
                                    <td><?php echo date("M d, Y", strtotime($itm->date_added)); ?></td>									
                                    <td style="text-align:right"><?php echo number_format($itm->amount,2); ?></td>
								
									<td style="text-align:right"><?php echo $type ; ?></td>
								
                                    
                                </tr>
                                <?php 
								$varAmt['amount'] += $itm->amount;
								
								$ctr++;
							} ?>
                        </tbody>
                        <tfoot>
                        	<tr style=" font-weight:bold;">
                            	
                                <td colspan="3" style="text-align:right">TOTAL :  <?php echo number_format($varAmt['amount'],2); ?></td>
								<td>&nbsp;</td>
                               
                                
                            </tr>
                        </tfoot>
                    </table>
					<?php */ ?>
                </div>
            </div>
            
            
 <script type="text/javascript">


function codes(){
		var table = $('#regcodesLists').dataTable( {
			"ajax": "<?=base_url()?>index.php/ewallet/incdetailsList",
			 scrollY: "1024px",			 
			 scrollCollapse: true,
			  "aLengthMenu": [100,500,1000],
			 "bDestroy":true,
			"columns": [
				{ "data": "id" },
				{ "data": "date" },				
				{ "data": "amount" , "class" :  "amt"},				
				{ "data": "type" , "class" :  "type" }
        	]			
		} );
		/*
		new $.fn.dataTable.FixedColumns( table, {
        	leftColumns: 2
    	} );
		*/
}
$(document).ready(function() {	
		codes();
	} );
</script>         
                