<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
	<!--style type="text/css">
		.form-horizontal .control-label{ width:180px;}
		.form-horizontal .controls { margin-left: 190px; }
		.ui-dialog, label, input, button, select, textarea {
			font-size: 12px;
			font-weight: normal;
			line-height: 12px;
		}
		.form-horizontal .control-group {
			margin-bottom: 5px;
		}
		input[type="text"]{
			padding:2px 6px;
		}
		.ui-dialog .ui-dialog-buttonpane button {
			padding:5px 8px;
		}
		.ui-dialog-titlebar-close{
			display:none;
		}
	</style-->
          <style type="text/css">
			.amt{
				text-align:right !important;
				font-size:10px;
			}
			.type{				
				font-size:10px;
			}
		  </style>
		  
            <!-- <div class="row-fluid">
                <div class="span12 greenback">
                    <h3 class="dash-title">E-wallet</h3>
                </div>
            </div> -->
            <!--END Content Box HEADER-->
        
        	<?php
				// print_r($data);
			?>    
            <div class="row-fluid">
				  <div class="span12">
					<table class="table table-striped table-hover table-bordered">
							<thead>
								<tr > 
									<th>Commission</th>
									<th>Total Amount</th>
									<th>Total Payout</th>
									<th>Available</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<td >Unilevel</td>
									<td ><?php echo number_format($data->total_comu,2); ?></td>
									<td ><?php echo number_format($data->total_payu,2); ?></td>
									<td ><?php echo number_format($data->total_comu-$data->total_payu,2); ?></td>
								</tr>
								
								<tr>
									<td >Leadership</td>
									<td ><?php echo number_format($data->total_coml,2); ?></td>
									<td ><?php echo number_format($data->total_payl,2); ?></td>
									<td ><?php echo number_format($data->total_coml-$data->total_payl,2); ?></td>
								</tr>
								<tr>
									<td >Room income </td>
									<td ><?php echo number_format($data->total_comr,2); ?></td>
									<td ><?php echo number_format($data->total_payr,2); ?></td>
									<td ><?php echo number_format($data->total_comr-$data->total_payr,2); ?></td>
								</tr>
								<tr>
							</tbody>
						</table>
				</div>
				<div>
					<button onclick="showModalForm()" class="btn btn-itop" style="margin-left:10px;">Encash E-wallet</button>
				</div>	
				
			<?php /* ?>
                <div class="span6">
                	
                	<table style="margin-left:20px;">
                    	<tr>
                        	<td style="font-weight:normal;">Total Commission Earned</td>
                            <td style=" width:10px; text-align:center">:</td>
                            <td><?php echo number_format($data->pending,2); ?></td>
                        </tr>
                        <tr>
                        	<td style="font-weight:normal;">Total Encashed</td>
                            <td style=" width:10px; text-align:center">:</td>
                            <td><?php echo number_format($data->encashed,2); ?></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr>
                        	<td style="font-weight:normal;">Total Available Ewallet Balance</td>
                            <td style=" width:10px; text-align:center">:</td>
                            <td><?php
								$totE = $data->availabled;
								echo number_format($totE,2); 
								?></td>
                        </tr>
                        <tr><td>&nbsp;</td></tr>
                        <tr><td colspan="3">
                           
                            <button onclick="showModalForm()" class="btn btn-success">Encash E-wallet</button>
                        </td></tr>
                    </table>
                    
                </div>
                
                 <div class="span6">
                	                    
                </div>
				<?php */ ?>
            </div>
            
            <div class="row-fluid"><div class="span12"></div></div>
            
                        
          <div class="row-fluid">
                <div class="span12">
                    <h3 class="dash-title" style="font-size:14px; font-weight:bold; padding-left:10px;color:#ebc200;">Encashment History</h3>
                </div>
            </div>
            
            
            
            <div class="row-fluid">
                <div class="span12" style="padding:0px 10px;">	
					
					 <table id="regcodesLists" class="table table-striped table-hover table-bordered" >
						<thead>
							<tr style="background:#FDFDFD;">
								<th style="width:15px">#</th>
								<th>MOP</th>      
								<th>Commission</th>                
								<th>Date Requested</th>                
								<th style="text-align:right">Amount</th>
								<th style="text-align:right">Processing Fee</th>
								<th style="text-align:right">Net Amount</th>
								<th style="text-align:right">Date Process</th>
							</tr>
						</thead>
						
					</table>
					
                </div>
            </div>
            
            
            
            <div id="myModal" title="Encash E-wallet" >
                    <div style="text-align:center; display:none" id="msgLoader">
                       <br /><br />
                       <div style="text-align:left; font-weight:bold; padding-left:20px; margin-bottom:20px;">Encashment on process.</div>
                       <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
                       <br /> <br />
                       <strong>
                            Do not close or refresh this browser. 
                            <br />
                            Please wait...
                        </strong>
                    </div>
                    <div class="modal-body" id="frmDiv">
                    	<form class="form-horizontal">
							<div class="control-group">
							  <label class="control-label">Payout</label>   	
							  <div class="controls">                                
									<select name="mop" id="mop">
										<option value="C">Cash</option>
										<option value="H">Check</option>										
									</select >
                             	</div>
							</div>	
                            <div class="control-group">
                                <label class="control-label" >Commission</label>                              
								 <div class="controls">                                
									<select name="comtype" id="comtype">
										<option value="U">Unilevel</option>
										<option value="R">Room Income</option>
										<option value="L"> Leadership</option>
									</select >
                             	</div>
                            </div>
                          
                            <div class="control-group">
                                <label class="control-label" >Amount Request </label>
                                <div class="controls">
                                	<input type="text" id="txtamt" value="" >
                             	</div>
                            </div>
                            
                           
                            
                        </form>
                    </div>
                    
            </div>
                
                
         <script type="text/javascript">
		
function codes(){
		var table = $('#regcodesLists').dataTable( {
			"ajax": "<?=base_url()?>index.php/ewallet/encashmentList",			
			 scrollY: "1024px",			 
			 scrollCollapse: true,
			  "aLengthMenu": [100,500,1000],
			 "bDestroy":true,
			"columns": [
				{ "data": "id" },
				{ "data": "mop" ,"class" :  "type"  },		
				{ "data": "type" ,"class" :  "type"  },		
				{ "data": "daterec" ,"class" :  "type"},				
				{ "data": "amount" , "class" :  "amt"},				
				{ "data": "processfee" , "class" :  "amt"},	
				{ "data": "net" , "class" :  "amt"},	
				{ "data": "dateproc"  ,"class" :  "type" }	
        	]			
		} );
		
		 		
		//new $.fn.dataTable.FixedColumns( table, {
        // 	leftColumns: 2
    	//} );
	
}

		 	$(document).ready(function(e) {
				codes();
				
				$( "#myModal" ).dialog({
					resizable: true,
					height:298,
					width:500,
					modal: true,
					autoOpen: false,
					closeOnEscape: false,
					buttons: {
					"Encash" : function() {
						
						
						//$( this ).dialog( "close" );
						if(parseFloat($("#txtamt").val()) >= 100){
							$(".ui-dialog-buttonset").css("display","none");
							$("#frmDiv").css("display","none");
							$("#msgLoader").css("display","");
							
							$.ajax({
								url : "<?=base_url()?>index.php/ewallet/sentMyEncashment",
								type: "POST",
								data : {"id":"<?php echo $this->session->userdata('id'); ?>", "mop":$( "#mop" ).val(), "amt":$( "#txtamt" ).val(), "comtype":$( "#comtype" ).val()},
								success:function(res, textStatus, jqXHR) 	{
									//alert(res);
									var arObj = JSON.parse(res);
									if(parseInt(arObj.error) == 1){
										alert(arObj.msg);
										window.location="<?=base_url()?>index.php/ewallet";
									} else {
										alert('Encashment has been sent.');
										//$( "#myModal" ).dialog( "close" );
										window.location="<?=base_url()?>index.php/ewallet";
									}
								},
								error: function(jqXHR, textStatus, errorThrown)	{
									alert(errorThrown);   
								}
							});
						} else {
							alert ("Insuficient Fund available, minimum net to encash is 100.");
						}
					},
					"Close" : function() {
					$( this ).dialog( "close" );
					}
					}
				});
					
            });
			
			function showModalForm(){
				$( "#myModal" ).dialog("open");
				$( "#txtamt" ).focus();
			}
		 </script>