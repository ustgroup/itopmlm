<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ewalletModel extends CI_Model {

		function __construct() {
			
			parent::__construct();
			$this->load->database();
		}	
		
		public function getSlot($userid=0){
			$querys="SELECT s.slot,m.fslot FROM rbs_muster_table  AS m
						LEFT JOIN rbs_slot AS s ON s.id=m.fslot
					 WHERE m.userid = '".$userid."' ";
			$query = $this->db->query($querys);	
			return $query->row();
			
		}
		
		public function getEncashmentHistory($userid=0){
			$querys="SELECT * FROM rbs_encashment WHERE userid = '".$userid."' ORDER BY request_date DESC, com_type ASC ";
			$query = $this->db->query($querys);	
			return $query->result() ;
		}
		
			public function getIncDetails($userid=0){
			$querys="SELECT * FROM `rbs_commision` WHERE recipient_id = '".$userid."' ORDER BY date_added ASC ";
			$query = $this->db->query($querys);	
			return $query->result() ;
		}
		
			public function getIncTotal($userid=0){
			$querys="SELECT SUM(amount) AS total FROM `rbs_commision` WHERE recipient_id = '".$userid."'  ";
			$query = $this->db->query($querys);	
			return $query->row() ;
		}
		
		public function getWallet($userid=0){
			
			$querys="SELECT * FROM rbs_muster_table 
					 WHERE userid = '".$userid."' ";
			$query = $this->db->query($querys);	
			$ewallet = $query->row();
			
			$arrayData = array();
			$arrayData['total_coml'] 	= $ewallet->total_coml;
			$arrayData['total_payl'] 	= $ewallet->total_payl;
			
			$arrayData['total_comr'] 	= $ewallet->total_comr;
			$arrayData['total_payr'] 	= $ewallet->total_payr;
			
			$arrayData['total_comu'] 	= $ewallet->total_comu;
			$arrayData['total_payu'] 	= $ewallet->total_payu;
			
			return (object) $arrayData;	
			
		}

		public function saveEncashment($amt,$comtype,$mop){
			$item = $this->getWallet($this->session->userdata('id'));
			if($comtype	=='L'){
				$Available = $item->total_coml - $item->total_payl;
			}elseif($comtype =='R'){
				$Available = $item->total_comr - $item->total_payr;
			}elseif($comtype =='U'){
				$Available = $item->total_comu - $item->total_payu;
			}
			
			
					
			if($Available >= $amt){
				$querys="INSERT INTO rbs_encashment(userid,request_date,amount,com_type,mop) 
											VALUES(
												'".$this->session->userdata('id')."' ,
												'".date("Y-m-d H:i:s")."' ,
												'".$amt."' ,
												'".$comtype."' ,
												'".$mop."' 
											)";
				if($this->db->query($querys)){
					
					//$updateQry = "UPDATE rbs_muster_table SET total_pay = total_pay + ".$amt." WHERE userid = '".$this->session->userdata('id')."' ";
					if($comtype	=='L'){
						$updateQry = "UPDATE rbs_muster_table SET total_payl = total_payl + ".$amt." WHERE userid = '".$this->session->userdata('id')."' ";
					}elseif($comtype =='R'){
						$updateQry = "UPDATE rbs_muster_table SET total_payr = total_payr + ".$amt." WHERE userid = '".$this->session->userdata('id')."' ";
					}elseif($comtype =='U'){
						$updateQry = "UPDATE rbs_muster_table SET total_payu = total_payu + ".$amt." WHERE userid = '".$this->session->userdata('id')."' ";
					}
					
					
					if($this->db->query($updateQry)){
						return 0;
					} else {
						return 3;
					}
				} else {
					return 2;
				}
			} else {
				return 1;
			}
		}
}