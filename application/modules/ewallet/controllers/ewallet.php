<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ewallet extends CI_Controller {

	public function __construct() {
		
		parent::__construct();  
		
	
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
				$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
				$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					
					
				$this->load->model('ewalletModel','model');
				$res['data'] = $this->model->getWallet($this->session->userdata('id'));
				//$res['items'] = $this->model->getEncashmentHistory($this->session->userdata('id'));
				$slot['slot'] = $this->model->getSlot($this->session->userdata('id'));
				$this->template->write_view('sidebar', 'main/sidebar',$slot);
				$this->template->write_view('content', 'ewallet',$res);
				$this->template->render();
			} else {
				redirect('admin');
			}
		} else {
			redirect('');
		}
	}
	
	public function incdetailsList(){
		if($this->session->userdata('group')=='M'){
			
			$this->load->model('ewalletModel','model');
			$items = $this->model->getIncDetails($this->session->userdata('id'));
			
			
			$arData = array();
			$ctr = 0;
			foreach($items as $item){	
				$ctr ++;
				$dt = date("M d, Y", strtotime($item->date_added));
				if($item->com_type =='L'){
					 $type = 'Leadership - Level '.$item->leader_level ;
				}elseif($item->com_type =='U'){
					 $type = 'Unilevel';
				}elseif($item->com_type =='R'){
					 $type = 'Room Income Table'.$item->tablef;
				}	
				
				$arData[] = array(
					"id" => $ctr,
					"date" => $dt  ,				
					"amount" => number_format($item->amount,2),
					"type" =>  $type 					
				);				
			}
			
			$results['data'] = $arData; //$this->model->affiliatesLists();
			echo json_encode($results);
		}
	}
	
	public function encashmentList(){
		if($this->session->userdata('group')=='M'){
			
			$this->load->model('ewalletModel','model');
			$items = $this->model->getEncashmentHistory($this->session->userdata('id'));
			
			
			$arData = array();
			$ctr = 0;
			foreach($items as $item){	
				$ctr ++;
				$daterec = date("M d, Y", strtotime($item->request_date));
				$dateproc = ($item->process_date > 0  ?  date("M d, Y", strtotime($item->process_date)) : 'pending' );
				if($item->com_type =='L'){
					 $type = 'Leadership' ;
				}elseif($item->com_type =='U'){
					 $type = 'Unilevel';
				}elseif($item->com_type =='R'){
					 $type = 'Room Income';
				}	
				$mop='';
				if($item->mop =='C'){
					 $mop = 'Cash' ;
				}elseif($item->mop =='H'){
					$mop = 'Check' ;
				}
				
				$arData[] = array(
					"id" => $ctr,
					"mop" => $mop  ,	
					"type" => $type  ,	
					"daterec" => $daterec  ,				
					"amount" => number_format($item->amount,2),
					"processfee" => number_format(100,2),
					"net" => number_format($item->amount-100,2),
					"dateproc" =>  $dateproc 		

						
				);				
			}
			
			$results['data'] = $arData; //$this->model->affiliatesLists();
			echo json_encode($results);
		}
	}
	
	public function incdetails() {
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
			
				$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
				$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
				$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
				
				$this->load->model('ewalletModel','model');					
				$res['items'] = $this->model->getIncTotal($this->session->userdata('id'));				
				$this->template->write_view('sidebar', 'main/sidebar');
				$this->template->write_view('content', 'incdetails',$res);	
				
				/*	
				$this->load->model('ewalletModel','model');				
				$res['items'] = $this->model->getIncDetails($this->session->userdata('id'));
				$slot['slot'] = $this->model->getSlot($this->session->userdata('id'));
				$this->template->write_view('sidebar', 'main/sidebar',$slot);
				$this->template->write_view('content', 'incdetails',$res);
				*/
				$this->template->render();
			} else {
				redirect('admin');
			}
		} else {
			redirect('');
		}
	}
	
	public function sentMyEncashment(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				//print_r($this->input->post('id'));
				if($this->session->userdata('id') == $this->input->post('id')){
					$postData = $this->input->post();	
					$this->load->model('ewalletModel','model');
					$res = $this->model->saveEncashment($postData['amt'],$postData['comtype'],$postData['mop']);
					if($res > 0){
						echo json_encode(array("error"=>1, "msg"=>"Insuficient Fund : invalid transaction!" ));
					} else {
						echo json_encode(array("error"=>0, "msg"=>"" ));
					}
				} else {
					echo json_encode(array("error"=>1, "msg"=>"Error invalid transaction!" ));
				}
			}
		} else {
			die;
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */