<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends CI_Controller {

	public function __construct() {
		parent::__construct();  
		$this->load->library('form_validation');
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
				$this->load->model('commissionModel','model');
			   //	$res['items'] = $this->model->ewalletLists();
				if($this->session->userdata('group')=='A'){
					$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
					$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
					$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','Member List');
					$this->template->write_view('navmenu', 'menu');
					$this->template->write_view('content', 'members');
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	
	public function memberList(){
		if($this->session->userdata('group')=='M'){
			redirect('');
		} else {
			$this->load->model('membersModel','model');
			$items = $this->model->memberList();
		
			$arData = array();
			//<a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tooltip on top">Tooltip on top</a>
				//"username" =>'<a href="#" data-taggle="tooltip"  data-placement ="top" title="password : '.$item->password.'">'.$item->username.'</a>',
			foreach($items as $item){				
				$regdate = date("M d, Y", strtotime($item->regdate));
				
				if($item->paidorfree == 'P'){
					$paidorfree = '<span>Paid</span>';
				}else{
					$paidorfree = '<span class="free">Free</span>';
				}
				$arData[] = array(
					"paidorfree" =>$paidorfree ,					
					"username" =>'<span  title="password : '.$item->password.'">'.$item->username.'</span>',
					"name" =>strtoupper($item->name),					
					"regdate" =>$regdate,
					"sponsor" =>$item->sponsor,
					"sponsorname" =>strtoupper($item->sponsorname)
				);				
			}
			
			$results['data'] = $arData; //$this->model->affiliatesLists();
			echo json_encode($results);
		}
	}
	
	 function delete() {	   	
			$postData = $this->input->post();						
			$error = 0;				
			$mes = '';			
			
			if( trim($this->session->userdata('group')) != trim($postData['usr'])  ){
				$error = 1;
				$mes = 'Required Fields!';
			}else{				
				
				$this->load->model('membersModel','model');		
				$this->model->deletemem();
				 $mes =  "Records Updated! ";					
				
			}
					
			$gg = array('error' => $error ,  'mes' => $mes );	
			echo json_encode($gg);	
			die();
   }
	
	 function reset() {	   	
			$postData = $this->input->post();						
			$error = 0;				
			$mes = '';			
			
			if( trim($this->session->userdata('group')) != trim($postData['usr'])  ){
				$error = 1;
				$mes = 'Required Fields!';
			}else{				
				
				$this->load->model('membersModel','model');		
				$this->model->resetmem();
				 $mes =  "Records Updated!";					
				
			}
					
			$gg = array('error' => $error ,  'mes' => $mes );	
			echo json_encode($gg);	
			die();
   }	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */