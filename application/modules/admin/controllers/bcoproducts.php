<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bcoproducts extends CI_Controller {

	public function __construct() {
		parent::__construct();  
		$this->load->library('form_validation');
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
			
				
				if($this->session->userdata('group')=='B'  ){
					$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
					$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
					$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','Products');
					$this->template->write_view('navmenu', 'bcomenu');
					$this->template->write_view('content', 'bcoproducts');
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	
	public function productsLists(){
		if($this->session->userdata('group')=='M'){
			redirect('');
		} else {
			$this->load->model('bcoproductsModel','model');
			$items = $this->model->productsLists();
		
			$arData = array();			
			foreach($items as $item){				
			
				$arData[] = array(
					"id" =>$item->id,
					"pname" => $item->pname,
					"pprice" => number_format($item->pprice,2),	
					"bprice" => number_format($item->bprice,2),	
					"srp" => number_format($item->srp,2),	
					"ppv" => $item->ppv,
					"pstock" => number_format($item->pstock,0)
					
				);				
			}
			
			$results['data'] = $arData; 
			echo json_encode($results);
		}
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */