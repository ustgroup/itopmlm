<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bco extends CI_Controller {

	public function __construct() {
		parent::__construct();  
		$this->load->library('form_validation');
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
			
				
				if($this->session->userdata('group')=='A'){
					$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
					$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
					$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','BCO');
					$this->template->write_view('navmenu', 'menu');
					$this->template->write_view('content', 'bco');
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	
	public function Lists(){
		if($this->session->userdata('group')=='M'){
			redirect('');
		} else {
			$this->load->model('bcoModel','model');
			$items = $this->model->Lists();
		
			$arData = array();			
			foreach($items as $item){				
			
				$arData[] = array(
					"id" =>$item->id,
					"username" => '<a href="#" onclick="edit('.$item->id.')" title = "Edit Info."> '.$item->username.' </a>',					
					"address" => $item->address,					
					"password" => $item->password,					
					
				);				
			}
			
			$results['data'] = $arData; 
			echo json_encode($results);
		}
	}
	
	public function edit(){
		
		if( $this->session->userdata('isLoggedIn') ) {
			$postData = $this->input->post();
			$this->load->model('bcoModel','model');							
			$res = $this->model->getbco($postData['pid']);
			echo json_encode(array("error"=>1, "username"=> $res->username, "password"=> $res->password, "address"=> $res->address ));
			
		}else{
		die();
		}
		
	}
	public function save(){
		if( $this->session->userdata('isLoggedIn') ) {
			if( $this->session->userdata('group')=='A' ){
				
				
					$postData = $this->input->post();	
					
					if ( empty($postData['username']) || empty($postData['password'])  || empty($postData['address'])  ){
							echo json_encode(array("error"=>1, "msg"=>"* Required Fields!" ));
							die();
					}else{
						if (!preg_match('/^[a-zA-Z0-9_-]+$/',$postData['username'])){
								echo json_encode(array("error"=>1, "msg"=>"Invalid username input!" ));
								die();
						}else{
								//control length of character
								if(strlen($postData['username']) < 3 || strlen($postData['username']) > 25) { 														
									echo json_encode(array("error"=>1, "msg"=>"User name - Minimum character 3!, Maximum character 25!" ));
									die();
								}
								//control password
								if(strlen($postData['password']) < 3 ) { 														
									echo json_encode(array("error"=>1, "msg"=>"Password - Minimum character 3!" ));
									die();
								}
								//control if user name already existed
								$this->load->model('bcoModel','model');	
								$res = $this->model->chkexist($postData['pid'],$postData['username']);
								
								if($res > 0){
									echo json_encode(array("error"=>1, "msg"=>"Username already existed!" ));	
									die();	
								} else {
									$res = $this->model->save($postData);
									echo json_encode(array("error"=>0, "msg"=>"Record Saved!" ));								
									die();
								}					
							}
						
					
						/*
						if($postData['pprice'] > 0 && $postData['ppv'] > 0 && $postData['pcost'] > 0 ){
							$valid=true;
						}else{
							$valid=false;
						}
							if($valid)	{
								$this->load->model('productsModel','model');							
								$res = $this->model->chkexist($postData['pid'],$postData['pname']);
								
								if($res > 0){
									echo json_encode(array("error"=>1, "msg"=>"Product name allready existed!" ));								
								} else {
									$res = $this->model->save($postData);
									echo json_encode(array("error"=>0, "msg"=>"Record Saved!" ));								
								}
						}else{
							echo json_encode(array("error"=>1, "msg"=>"Invalid character input!" ));	
						}
						*/
					}
				
				
			}
		} else {
			die;
		}
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */