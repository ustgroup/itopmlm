<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payout extends CI_Controller {

	public function __construct() {
		parent::__construct();  
		$this->load->library('form_validation');
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
				$this->load->model('payoutModel','model');
				$res['items'] = $this->model->getEncashment();
				$res['batch'] = $this->model->getBatch();
				if($this->session->userdata('group')=='A'){
					//$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
					//$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
					//$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','Payout Request');
					$this->template->write_view('navmenu', 'menu');
					$this->template->write_view('content', 'payout', $res);
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	
	
	
	public function processCurrentPayout(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
				//print_r($this->input->post('id'));
				if($this->session->userdata('id') == $this->input->post('id')){	
					$this->load->model('payoutModel','model');
					$res = $this->model->processPayout();
					if($res > 0){
						echo json_encode(array("error"=>1, "msg"=>"Error ".$res." : payout cannot be process!" ));
					} else {
						echo json_encode(array("error"=>0, "msg"=>"" ));
					}
				} else {
					echo json_encode(array("error"=>1, "msg"=>"Error invalid transaction!" ));
				}
				
			}
		} else {
			die;
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */