<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

	public function __construct() {
		parent::__construct();  
		$this->load->library('form_validation');
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
			
				
				if($this->session->userdata('group')=='A'  ){
					$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
					$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
					$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','Products');
					$this->template->write_view('navmenu', 'menu');
					$this->template->write_view('content', 'products');
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	
	public function productsLists(){
		if($this->session->userdata('group')=='M'){
			redirect('');
		} else {
			$this->load->model('productsModel','model');
			$items = $this->model->productsLists();
		
			$arData = array();			
			foreach($items as $item){				
			
				$arData[] = array(
					"id" =>$item->id,
					"pname" => '<a href="#" onclick="editprod('.$item->id.')" title = "Edit Product"> '.$item->pname.' </a>',
					"pcost" => number_format($item->pcost,2),										
					"pprice" => number_format($item->pprice,2),	
					"bprice" => number_format($item->bprice,2),	
					"srp" => number_format($item->srp,2),	
					"ppv" => $item->ppv,
					"pstock" => number_format($item->pstock,0)
					
				);				
			}
			
			$results['data'] = $arData; 
			echo json_encode($results);
		}
	}
	
	public function edit(){
		
		if( $this->session->userdata('isLoggedIn') ) {
			$postData = $this->input->post();
			$this->load->model('productsModel','model');							
			$res = $this->model->getprod($postData['pid']);
			echo json_encode(array("error"=>1, "pname"=> $res->pname, "pcost"=> $res->pcost, "pprice"=> $res->pprice, "bprice"=> $res->bprice, "srp"=> $res->srp, "ppv"=> $res->ppv ));
			
		}else{
		die();
		}
		
	}
	public function save(){
		if( $this->session->userdata('isLoggedIn') ) {
			if( $this->session->userdata('group')=='A' ){
				
				
					$postData = $this->input->post();	
					
					if ( empty($postData['pname']) || empty($postData['pcost'])  || empty($postData['pprice']) || empty($postData['ppv']) ){
							echo json_encode(array("error"=>1, "msg"=>"* Required Fields!" ));
					}else{
					
						if($postData['pprice'] > 0 && $postData['ppv'] > 0 && $postData['pcost'] > 0 ){
							$valid=true;
						}else{
							$valid=false;
						}
							if($valid)	{
								$this->load->model('productsModel','model');							
								$res = $this->model->chkexist($postData['pid'],$postData['pname']);
								
								if($res > 0){
									echo json_encode(array("error"=>1, "msg"=>"Product name allready existed!" ));								
								} else {
									$res = $this->model->save($postData);
									echo json_encode(array("error"=>0, "msg"=>"Record Saved!" ));								
								}
						}else{
							echo json_encode(array("error"=>1, "msg"=>"Invalid character input!" ));	
						}
					}
				
				
			}
		} else {
			die;
		}
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */