<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stock extends CI_Controller {

	public function __construct() {
		parent::__construct();  
		$this->load->library('form_validation');
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
			
				
				if($this->session->userdata('group')=='A'){
					$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
					
					$this->template->add_css('themes/jc_themes/chosen/chosen.css');
					$this->template->add_js('themes/jc_themes/chosen/chosen.jquery.js');
					
					$this->template->add_js('themes/jc_themes/jquery-ui/jquery.numeric.js');
					$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
					$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					$this->template->set_master_template('../../themes/jc_admin');
					
					$this->load->model('stockModel','model');
					$res['data'] = $this->model->productLists();
				
					$this->template->write('header','Product Stock');
					$this->template->write_view('navmenu', 'menu');
					$this->template->write_view('content', 'stock',$res);
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	
	public function productsLists(){
		if($this->session->userdata('group')=='M'){
			redirect('');
		} else {
			$this->load->model('stockModel','model');					
			$items = $this->model->productLists();			
			echo json_encode($items);
		}
	}
	
	public function stockLists(){
		if($this->session->userdata('group')=='M'){
			redirect('');
		} else {
			//$postData = $this->input->get();
			
			$this->load->model('stockModel','model');
			$items = $this->model->stockLists();
		
			$arData = array();			
			foreach($items as $item){				
				$regdate = date("M d, Y", strtotime($item->dates));
				$arData[] = array(										
					"tamt" => number_format($item->tamt,2),															
					"dates" =>$regdate,
					"refno" => '<a href="#" onclick="viewdetails('."'".$item->refno."'".')" title = "View Details"> '.$item->refno.' </a>'
				);				
			}
			
			$results['data'] = $arData; 
			echo json_encode($results);
		}
	}
	
	public function getmember(){
		
		if( $this->session->userdata('isLoggedIn') ) {
			$postData = $this->input->post();
			$this->load->model('stockModel','model');							
			/*
			$res = $this->model->memberLists();			
			$tbl='<option value="0">Select Member</option>';
			foreach($res AS $itm){				
				$tbl.='<option value="'.$itm->userid.'">'.$itm->username.' '.$itm->name.'</option>';						
			}	
			*/
			$res = $this->model->productLists();
			
			$prd='<option value="0">Select Product</option>';
			foreach($res AS $itm){				
				$prd.='<option value="'.$itm->id.'">'.$itm->pname.'</option>';						
			}
			
			//echo json_encode(array("error"=>1, "member"=> $tbl, "product"=> $prd ));
			echo json_encode(array("error"=>1,"product"=> $prd ));
			
		}else{
		die();
		}
		
	}
	
	public function details(){
		
		if( $this->session->userdata('isLoggedIn') ) {
			$postData = $this->input->post();
			$this->load->model('stockModel','model');							
			$res = $this->model->getdetails($postData['refno']);
			
			$tbl='Reference # : <span id="spanefno">'.$postData['refno'].'</span><br><br><table style="width:100%">
				  
			
			 
            <tr class="crow">				
                <td class="drow"><strong>Product Name</strong></td>
				<td class="amt drow"><strong>Qty</strong></td>
				<td class="amt drow"><strong>Product Cost</strong></td>
				<td class="amt drow"><strong>Total Amount</strong></td>
				
            </tr>
       
			';
			$tamt = 0;
			$tpv = 0;
			foreach($res AS $itm){
				$tamt += $itm->qty * $itm->pprice ;				
				$tbl.='<tr class="crow">
						<td class="drow">'.$itm->pname.'</td>
						<td class="amt drow">'.$itm->qty.'</td>'.'
						<td class="amt drow">'.number_format($itm->pprice,2).'</td>'.'
						<td class="amt drow">'.number_format($itm->qty * $itm->pprice,2).'</td>'.'						
					</tr>';
			}
			$tbl.='<tr class="crow">
					<td>	</td><td>	</td><td>	</td>
					<td class="amt "><strong>'.number_format($tamt,2).'</strong></td>					
					</tr>';
			$tbl.='</table>';
			
			echo json_encode(array("error"=>1, "details"=> $tbl ));
			
		}else{
		die();
		}
		
	}
	public function save(){
		if( $this->session->userdata('isLoggedIn') ) {
			if( $this->session->userdata('group')=='A' ){
				
				
					$postData = $this->input->post();	
					
					if ( empty($postData['datec']) || empty($postData['refno'])  ){
							echo json_encode(array("error"=>1, "msg"=>"* Required Fields!" ));
					}else{
					
						
						$validid=0;
						$validqty=0;
						
						for($x = 0;$x<= count( $postData['prod'] ) - 1;$x++ ){
							$validid = $validid + $postData['prod'][$x];
							$validqty =  $validqty +  $postData['qty'][$x];
						
						}
	
						$valid=true;
					
						if($validid == 0){
							$valid=false;	
							echo json_encode(array("error"=>1, "msg"=>"Invalid  Product!" ));	
							die();
						}
						
						if($validqty ==0){
							$valid=false;	
							echo json_encode(array("error"=>1, "msg"=>"Invalid Qty!" ));			
							die();
							
						}
					
						if($valid)	{
							$this->load->model('stockModel','model');							
							$res = $this->model->chkrefnoexist($postData['refno']);
								if($res > 0){
									echo json_encode(array("error"=>1, "msg"=>"Ref. No. allready existed!" ));								
								} else {
									$res = $this->model->save($postData);
									echo json_encode(array("error"=>0, "msg"=>"Record Saved!" ));
								}	
							
						}	
						
					}
				
				
			}
		} else {
			die;
		}
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */