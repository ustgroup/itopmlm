<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Monthlyp extends CI_Controller {

	public function __construct() {
		parent::__construct();  
		$this->load->library('form_validation');
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
				//$this->load->model('commissionModel','model');
			   //	$res['items'] = $this->model->ewalletLists();
				if($this->session->userdata('group')=='A'){
					$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
					$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
					$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','Monthly Process');
					$this->template->write_view('navmenu', 'menu');
					$this->template->write_view('content', 'monthlyp');
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	
	public function monthlyList(){
		if($this->session->userdata('group')=='M'){
			redirect('');
		} else {
			$this->load->model('monthlypModel','model');
			$items = $this->model->monthyList();
		
			$arData = array();
			
			foreach($items as $item){				
				
				
				if($item->pmonth=='01'){
					$Month = 'January';
				}elseif($item->pmonth=='02'){
					$Month = 'February';
				}elseif($item->pmonth=='03'){
					$Month = 'March';
				}elseif($item->pmonth=='04'){
					$Month = 'April';
				}elseif($item->pmonth=='05'){
					$Month = 'May';
				}elseif($item->pmonth=='06'){
					$Month = 'June';
				}elseif($item->pmonth=='07'){
					$Month = 'July';
				}elseif($item->pmonth=='08'){
					$Month = 'August';
				}elseif($item->pmonth=='09'){
					$Month = 'September';
				}elseif($item->pmonth=='10'){
					$Month = 'October';
				}elseif($item->pmonth=='11'){
					$Month = 'November';
				}elseif($item->pmonth=='12'){
					$Month = 'December';
				}
				
				$arData[] = array(
					"username" =>$item->username,
					"name" =>$item->name,
					"ownpv" =>number_format($item->ownpv,0),
					"lpointsr" =>number_format($item->lpointsr,0),
					"rpointsr" =>number_format($item->rpointsr,0),
					"month" => $Month,
					"year" =>$item->pyear
				);				
			}
			
		
			
			$results['data'] = $arData; //$this->model->affiliatesLists();
			echo json_encode($results);
		}
	}
	

	public function process(){
		if( $this->session->userdata('isLoggedIn') ) {
			if( $this->session->userdata('group')=='A' ){
				
				
				$postData = $this->input->post();						
				$this->load->model('monthlypModel','model');							
				$res = $this->model->chkexist($postData['month'],$postData['year']);
					if($res > 0){
						echo json_encode(array("error"=>1, "msg"=>"This month already processed! " ));								
					} else {
						$res = $this->model->process_pairing($postData);
						echo json_encode(array("error"=>0, "msg"=>"Record successfully updated!" ));
					}	
				
									
			}
		} else {
			die;
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */