<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Regcodes extends CI_Controller {

	public function __construct() {
		parent::__construct();  
		$this->load->library('form_validation');
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
			
				
				if($this->session->userdata('group')=='A'){
					$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
					$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
					$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','Registration Code');
					$this->template->write_view('navmenu', 'menu');
					$this->template->write_view('content', 'regcodes');
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	
	public function regcodesList(){
		if($this->session->userdata('group')=='M'){
			redirect('');
		} else {
			$this->load->model('regcodesModel','model');
			$items = $this->model->regcodesLists();
		
			$arData = array();
			
			foreach($items as $item){				
				if($item->paidorfree == 'P'){
					$paidorfree = '<span>Paid</span>';
				}else{
					$paidorfree = '<span class="free">Free</span>';
				}
				
				$arData[] = array(
					"id" => $item->id ,
					"remark" => $item->remark ,						
					"paidorfree" => $paidorfree,		
					"fcode" => $item->fcode ,						
					"username" => $item->username,
					"name" => $item->name,
					"dateencode" => $item->dateencode
				);				
			}
			
			$results['data'] = $arData; //$this->model->affiliatesLists();
			echo json_encode($results);
		}
	}
	
	  function addcodes() {	   	
			$postData = $this->input->post();						
			$error = 0;				
			$mes = '';			
			
			if($postData['qty'] > 0){
				$validqty=true;
			}else{
				$validqty=false;
			}
			if( $this->session->userdata('group') != 'A' || $validqty == false ){
				$error = 1;
				$mes = 'Required Fields!';
			}else{				
				
				$this->load->model('regcodesModel','model');		
				$this->model->addcodes($postData['qty'],$postData['paidorfree'],$postData['remark']);				
				$mes =  "Registration Code Updated! ";					
				
			}
					
			$gg = array('error' => $error ,  'mes' => $mes );	
			echo json_encode($gg);	
			die();
   }
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */