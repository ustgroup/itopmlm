<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Affiliates extends CI_Controller {

	public function __construct() {
		parent::__construct();  
		$this->load->library('form_validation');
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
				//$this->load->model('affiliatesModel','model');
			   	//$res['items'] = $this->model->affiliatesLists();
				if($this->session->userdata('group')=='A'){
					$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
					$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
					$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','Affiliates');
					$this->template->write_view('navmenu', 'menu');
					$this->template->write_view('content', 'affiliates');
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	
	public function affiliateList(){
		if($this->session->userdata('group')=='M'){
			redirect('');
		} else {
			$this->load->model('affiliatesModel','model');
			$items = $this->model->affiliatesLists();
			/*
			foreach($items as $item){
				for($i=0; $i<=1000; $i++){
					$ndata[] = $item;
				}
			}
			*/
			$arData = array();
			foreach($items as $item){
				$payStr ='';
				switch($item->member_status){
					case 1 : $payStr ='Over the counter'; break;
					case 2 : $payStr ='Dragon pay'; break;
					case 3 : $payStr ='Online Credit Card'; break;
					case 4 : $payStr ='Totally Free'; break;
					case 5 : $payStr ='Free w/ recuite'; break;
				}
				//for($i=0; $i<=20; $i++){
				$arData[] = array(
								"username" => $item->username ,
								"fullname" => $item->l_name.' '.$item->f_name.', '.$item->m_name ,
								"regdate" => date("M d, Y" , strtotime($item->reg_date)) ,
								"act_type" => $item->member_type ,
								"pay_type" => $payStr,
								"city" => $item->city , 
								"province" => $item->province ,
								"country" => $item->country 
							);
				//}
			}
			$results['data'] = $arData; //$this->model->affiliatesLists();
			echo json_encode($results);
		}
	}
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */