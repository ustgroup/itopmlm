<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commission extends CI_Controller {

	public function __construct() {
		parent::__construct();  
		$this->load->library('form_validation');
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
				$this->load->model('commissionModel','model');
			   //	$res['items'] = $this->model->ewalletLists();
				if($this->session->userdata('group')=='A'){
					$this->template->add_js('themes/jc_themes/datatables/media/js/jquery.dataTables.min.js');
					$this->template->add_js('themes/jc_themes/datatables/media/extension/dataTables.fixedColumns.js');
					$this->template->add_css('themes/jc_themes/datatables/media/css/jquery.dataTables.min.css');
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','Member E-wallet');
					$this->template->write_view('navmenu', 'menu');
					$this->template->write_view('content', 'commission');
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	/*
	$querys="SELECT s.slot,m.total_com,m.total_pay,m.username,CONCAT(m.fname,' ', m.lname) AS name FROM rbs_muster_table  AS m 
			LEFT JOIN rbs_slot AS s ON s.id=m.fslot
			*/
	public function ewalletList(){
		if($this->session->userdata('group')=='M'){
			redirect('');
		} else {
			$this->load->model('commissionModel','model');
			$items = $this->model->ewalletList();
		
			$arData = array();
			
			foreach($items as $item){				
				$avail = $item->total_com-$item->total_pay;
				$arData[] = array(
					"username" =>$item->username,
					"name" =>$item->name,				
					"total_coml" =>number_format($item->total_coml,2),
					"total_comr" =>number_format($item->total_comr,2),
					"total_comu" =>number_format($item->total_comu,2),
					
					"total_payl" =>number_format($item->total_payl,2),					
					"total_payu" =>number_format($item->total_payu,2),					
					"total_payr" =>number_format($item->total_payr,2),					
					"avail_fund" =>number_format($avail,2)					
				);				
			}
			
			$results['data'] = $arData; //$this->model->affiliatesLists();
			echo json_encode($results);
		}
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */