<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct() {
		
		parent::__construct();  
		
		$this->load->library('form_validation');
	
	}
	
	public function index(){
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				redirect('');
			} else {
				if($this->session->userdata('group')=='A'){
					$this->load->model('adminModel','model');
					$res['data'] = $this->model->count_member();
			   
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','Dashboard');
					$this->template->write_view('navmenu', 'menu');
					
					$this->template->write_view('content', 'dashboard',$res);
					$this->template->render();
				}elseif($this->session->userdata('group')=='B'){
					//$this->load->model('adminModel','model');							   
					$this->template->set_master_template('../../themes/jc_admin');
					$this->template->write('header','Dashboard');
					$this->template->write_view('navmenu', 'bcomenu');
					
					$this->template->write_view('content', 'bcodashboard');
					$this->template->render();
				}
			}
		} else { 
			redirect('');
		}
	}
	
	
	function ifExist($str){
		$this->load->model('mainModel','main');
	   	$res = $this->main->check_exists($this->input->post('userid'),$this->input->post('userPassword'));
	}
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */