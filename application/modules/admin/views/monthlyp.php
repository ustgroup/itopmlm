<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
	.amt{
		text-align:right !important;
	}
	
</style>


<button name="delete"  class="btn btn-warning" onclick="$('#modalCompute').dialog( 'open' );">&nbsp;Process&nbsp;</button>
<!--button name="reset"  class="btn btn-warning" onclick="xreset()">&nbsp;Reset&nbsp;</button-->
<div style="width:100%;padding-top:5px">
	
    <table id="monthlyList" class="table table-striped table-hover table-bordered" >
        <thead>
            <tr style="background:#FDFDFD;">
				<th>Username</th>
                <th>Name</th>
				<th>Maintenance PV</th>
				<th>Left Points</th>
                <th>Right Points</th>           				
				<th>Month</th>
                <th>Year</th>
            </tr>
        </thead>
        
    </table>
</div>

<?php $m = date('m');$y = date('Y') ; ?>
<div id="modalCompute" title=" Monthly Process">
	<table>
		<tr>
			<td valign="middle">
			Month : <select name="month" id="month" style="width:150px">
						<option value="01" <?php echo ( $m =='01' ? 'selected="selected"' : '' ); ?>>January</option>
						<option value="02" <?php echo ( $m =='02' ? 'selected="selected"' : '' ); ?>>February</option>
						<option value="03" <?php echo ( $m =='03' ? 'selected="selected"' : '' ); ?>>March</option>
						<option value="04" <?php echo ( $m =='04' ? 'selected="selected"' : '' ); ?>>April</option>
						<option value="05" <?php echo ( $m =='05' ? 'selected="selected"' : '' ); ?>>May</option>
						<option value="06" <?php echo ( $m =='06' ? 'selected="selected"' : '' ); ?>>June</option>
						<option value="07" <?php echo ( $m =='07' ? 'selected="selected"' : '' ); ?>>July</option>
						<option value="08" <?php echo ( $m =='08' ? 'selected="selected"' : '' ); ?>>August</option>
						<option value="09" <?php echo ( $m =='09' ? 'selected="selected"' : '' ); ?>>September</option>
						<option value="10" <?php echo ( $m =='10' ? 'selected="selected"' : '' ); ?>>October</option>
						<option value="11" <?php echo ( $m =='11' ? 'selected="selected"' : '' ); ?>>November</option>
						<option value="12" <?php echo ( $m =='12' ? 'selected="selected"' : '' ); ?>>December</option>
					</select >
			Year : <select name="year" id="year" style="width:100px">
						<option value="<?php echo $y-1; ?>" <?php echo ( $y ==$y-1 ? 'selected="selected"' : '' ); ?>><?php echo $y-1; ?></option>
						<option value="<?php echo $y; ?>" <?php echo ( $y ==$y ? 'selected="selected"' : '' ); ?>><?php echo $y; ?></option>
						<option value="<?php echo $y+1; ?>" <?php echo ( $y ==$y+1 ? 'selected="selected"' : '' ); ?>><?php echo $y+1; ?></option>						
					</select >
			</td>
			
		</tr>
</table>

	<div id="proc" style="display:none">
    <p align="center" style="color:red">
    	Monthly transaction on process.
    </p>
    <p align="center">
         <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
    </p>
    <p align="center">
        <strong>Don't 	Refresh or close this browser.</strong><br />
       <span style="color:red"> Please wait... </span>
    </p>
    
	</div>
</div>

<!--div id="dialog" title="Please Wait..." style="display:none">
			<div style="text-align:center;width:100%">  
           <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
			<br /> 			
                Do not close or refresh this browser.                
            </div>
</div-->
<script type="text/javascript">
$(document).ready( function () {
	
	$( "#modalCompute" ).dialog({
			autoOpen: false,
			height: 300,
			width: 400,
			modal: true,
			buttons: {
				Proccess: function() {					
					
					$(".ui-dialog-buttonset").css("display", "none");
					$("#proc").css("display", "block");
					
					$.ajax({
								url : "<?=base_url()?>index.php/admin/monthlyp/process",
								type: "POST",
								data : {"month":$( "#month" ).val() , "year":$( "#year" ).val() },
								success:function(res, textStatus, jqXHR) 	{
									//alert(res);
									var arObj = JSON.parse(res);
									
									$(".ui-dialog-buttonset").css("display", "block");
									$("#proc").css("display", "none");
					
									if(parseInt(arObj.error) == 1){
										alert(arObj.msg);										
										//window.location="<?=base_url()?>index.php/ewallet";
									} else {
										alert(arObj.msg);																				
										//$( "#prodModal" ).dialog("close");
										//codes();
										window.location="<?=base_url()?>index.php/admin/monthlyp";
									}
									
								},
								error: function(jqXHR, textStatus, errorThrown)	{
									alert(errorThrown);   
								}
							});
					
					/*
					formdata = new FormData();      	
					if (formdata) {										
						formdata.append("prc","1" );
						formdata.append("month",$("#month").val() );
						formdata.append("year",$("#year").val() );
						
							jQuery.ajax({
							url: "<?php echo base_url() . 'index.php/admin/proc_commission'; ?>",
							type: "POST",
							datatype:"json",
							data: formdata,
							processData: false,
							contentType: false,
							success:function(res){
								var result =jQuery.parseJSON(res);															
								if(result.error == 1){	
									alert(result.mes);									
								}else{
									alert(result.mes);																
								}
								$('#modalCompute').dialog( 'close' );
							}
						});							
					}
					*/
				},
				 Cancel: function() {
					$( this ).dialog( "close" );
					$("#proc").css("display", "none");
				}
			}
		});

	} );		
		
function xreset(){	
	if(confirm("Reset Members? ")){
		$( "#dialog" ).dialog({ modal: true});	
		
		 $('button[name="reset"]').attr('disabled','disabled');		
			formdata = new FormData();      	
					if (formdata) {																	
							formdata.append("usr", '<?php echo $this->session->userdata('group'); ?> ');							
						
							jQuery.ajax({
							url: "<?=base_url()?>index.php/admin/members/reset",
							type: "POST",
							datatype:"json",
							data: formdata,
							processData: false,
							contentType: false,
							success:function(res){
								var result =jQuery.parseJSON(res);															
								$( "#dialog" ).dialog( "close" );	
								if(result.error == 1){	
									alert(result.mes);									
								}else{
									alert(result.mes);
									//window.location = '<?=base_url()?>index.php/admin/regcodes';	
									codes();	
								}
								
								$('button[name="reset"]').removeAttr('disabled');	
							}
						});						
					}
			
	}
	
}


function codes(){
		var table = $('#monthlyList').dataTable( {
			"ajax": "<?=base_url()?>index.php/admin/monthlyp/monthlyList",
			 scrollY: "850px",
			 scrollX: true,
			 scrollCollapse: true,
			 "bDestroy":true,
			 "order": [ 5, "asc" ],
			  "order": [ 6, "asc" ],
			"columns": [
				{ "data":"username"},
				{ "data":"name"},				
				{ "class":"amt","data":"ownpv"},
				{ "class":"amt","data":"lpointsr"},
				{ "class":"amt","data":"rpointsr"},
				{ "data":"month"},
				{ "data":"year"}				
        	]			
		} );
		//new $.fn.dataTable.FixedColumns( table, {
        //	leftColumns: 2
    	//} );
}
$(document).ready(function() {	
		codes();
		
	} );
</script>