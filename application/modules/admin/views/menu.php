<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
           <ul class="nav nav-pills nav-hover" role="menu" style=" float:right" >
                	<li><a href="<?=base_url()?>index.php/admin">Home</a></li>
					<li style="color:#FFF; padding-top:5px;">|</li>	
					 <li class="dropdown-submenu">
                    	<a tabindex="-2"  href="#">Codes</a>
                        <ul class="dropdown-menu">
                           	<li><a href="<?=base_url()?>index.php/admin/regcodes">Registration Code</a></li>
                           	<li class="divider"></li>
                            
                        </ul>
                    </li>
					
                    <!--li style="color:#FFF; padding-top:5px;">|</li>
                    <li><a href="<?=base_url()?>index.php/admin/affiliates">Affiliates</a></li-->
					
                    <li style="color:#FFF; padding-top:5px;">|</li>
                    <li class="dropdown-submenu">
                        <a tabindex="-1" href="#">Commission</a>
                        <ul class="dropdown-menu">                           	
							
							<li>
                            	<a href="<?=base_url()?>index.php/admin/payout">Payout Request</a>                                
                            </li>
							  	<li class="divider"></li>                        								
                        	<li>
                            	<a href="<?=base_url()?>index.php/admin/commission">Member E-wallet</a>
                                
                            </li>
                          	<!--li class="divider"></li>                        								
                        	<li>
                            	<a href="<?=base_url()?>index.php/admin/monthlyp">Monthly Process</a>                                
                            </li-->
                        
                        </ul>
                    </li>
                 
					<li style="color:#FFF; padding-top:5px;">|</li>	
					 <li class="dropdown-submenu">
                    	<a tabindex="-2"  href="#">Members</a>
                        <ul class="dropdown-menu">
                           	<li><a href="<?=base_url()?>index.php/admin/members">Members List</a></li>
                           	<li class="divider"></li>
                            
                        </ul>
                    </li>
					
					<!--li style="color:#FFF; padding-top:5px;">|</li>	
					 <li class="dropdown-submenu">
                    	<a tabindex="-2"  href="#">Product</a>
                        <ul class="dropdown-menu">
                           	<li><a href="<?=base_url()?>index.php/admin/products">Product List</a></li>
                           	<li class="divider"></li>
								<li><a href="<?=base_url()?>index.php/admin/stock">Add Stock</a></li>
                           	<li class="divider"></li>
                            <li class="dropdown-submenu">
							<a tabindex="-2"  href="#">Sales</a>
								<ul class="dropdown-menu">
									<li><a href="<?=base_url()?>index.php/admin/sales">Member</a></li>
									<li class="divider"></li>
									
								</ul>
							</li>
                        </ul>
                    </li-->
					
                    <li style="color:#FFF; padding-top:5px;">|</li>
                    <li>
					<a href="#" onclick="showPWForm()">Change Password</a>
					</li>
					
                    <li style="color:#FFF; padding-top:5px;">|</li>
                    <li><a href="<?=base_url()?>index.php/logout">Logout</a></li>
                </ul>
            
				
		
   	<script type="text/javascript">
		
		 	$(document).ready(function(e) {
				
				$( "#xmyModal" ).dialog({
					resizable: true,
					height:290,
					width:500,
					modal: true,
					autoOpen: false,
					closeOnEscape: false,
					buttons: {
					"Update" : function() {
						
						
							//$( this ).dialog( "close" );						
							//$(".ui-dialog-buttonset").css("display","none");
							//$("#xfrmDiv").css("display","none");
							$("#xmsgLoader").css("display","");
							
							$.ajax({
								url : "<?=base_url()?>index.php/main/updatePW",
								type: "POST",
								data : {"id":"<?php echo $this->session->userdata('id'); ?>", "oldpw":$( "#oldpw" ).val(), "newpw":$( "#newpw" ).val(), "conpw":$( "#conpw" ).val() },
								success:function(res, textStatus, jqXHR) 	{
									//alert(res);
									var arObj = JSON.parse(res);
									if(parseInt(arObj.error) == 1){
										alert(arObj.msg);
										//window.location="<?=base_url()?>index.php/ewallet";
									} else {
										alert(arObj.msg);										
										//window.location="<?=base_url()?>index.php/ewallet";
									}
									$("#xmsgLoader").css("display","none");
								},
								error: function(jqXHR, textStatus, errorThrown)	{
									alert(errorThrown);   
								}
							});
							
					},
					"Close" : function() {
					$( this ).dialog( "close" );
					}
					}
				});
					
            });
				function showPWForm(){
				
				$( "#xmyModal" ).dialog("open");
				
			}
		
			
		 </script>	