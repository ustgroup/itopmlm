<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
	
</style>
<?php $postData = $this->input->post();	
 ?>
<div class="row-fluid">
	<div class="span6" style="margin:5px 10px 10px">
    	<button class="btn btn-warning" onclick="callModal()" disabled="disabled">Process Payout Request</button>
    </div>
	<div class="span6" style="margin:0px 0px 0px;text-align:right">
    	<form action="<?=base_url()?>index.php/admin/payout" method="post">
		<select name="batch" id="batch" onchange="this.form.submit()">
			<option value="">Pending</option>
			 <?php 
                $ctr = 1;                
                foreach($batch as $itm) { 
                    ?>					
			<option value="<?php  echo $itm->process_date; ?>" <?php echo ($postData['batch']==$itm->process_date ? 'selected="selected"' : ' '); ?> >Batch <?php  echo $ctr. ' '.date("M d, Y", strtotime($itm->process_date)); ?></option>			
			<?php  $ctr++; } ?>
		</select>
		</form>
    </div>
	
</div>
<div class="row-fluid">
	<div class="span12">
        <table class="table table-striped table-condensed table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Commission</th>
					<th>Username</th>
                    <th>Name</th>
                    <th>Date Requested</th>
                    <th>Amount</th>    
					<th style="text-align:right">Processing Fee</th>					
					<th style="text-align:right">Net Amount</th>	
                </tr>
            </thead>
            <tbody>
                <?php 
                $ctr = 1;
                $varAmt = array();
                $varAmt['amount'] = 0; 
                foreach($items as $itm) { 
					$tax = $itm->amount*0.10;
					$net = $itm->amount-100;
						
					if($itm->com_type	=='L'){
						$comtype = 'Leadership';
					}elseif($itm->com_type =='R'){
						$comtype = 'Room Income';
					}elseif($itm->com_type =='U'){
						$comtype = 'Unilevel';
					}
			
                    ?>
                    <tr>
                        <td><?php echo $ctr; ?></td>
                        <td><?php echo $comtype; ?></td>
						<td><?php echo $itm->username; ?></td>
                        <td><?php echo $itm->fullname; ?></td>
                        <td><?php echo date("M d, Y", strtotime($itm->request_date)); ?></td>
                        <td><?php echo number_format($itm->amount,2); ?></td>
						<td style="text-align:right"><?php echo number_format(100,2); ?></td>						
						<td style="text-align:right"><?php echo number_format($itm->amount-100,2); ?></td>
                        
                    </tr>
                    <?php 
                    $varAmt['amount'] += $net;

                    $ctr++;
                } ?>
            </tbody>
            <tfoot>
                <tr><td colspan="9"></td></tr>
                <tr style=" font-weight:bold;">
                   <td colspan="8" style="text-align:right">TOTAL :  <?php echo number_format($varAmt['amount'],2); ?></td>
                </tr>
            </tfoot>
        </table>
	</div>
</div>
<?php
/*
<tr>
	<td><?php echo $item->username; ?></td>
	<td><?php echo $item->f_name; ?></td>
	<td><?php echo $item->l_name; ?></td>
	<td><?php echo $item->m_name; ?></td>
	<td><?php echo $item->member_type; ?></td>
	<td><?php echo date("M d, Y",strtotime($item->reg_date)); ?></td>
</tr>
*/
?>

    
     
    <!-- Modal -->
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
    <!--button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button-->
    <h3 id="myModalLabel">Process Payout</h3>
    </div>
    <div class="modal-body">
    	<div style="text-align:center; display:none" id="msgLoader">
           <div style="text-align:left; font-weight:bold; padding-left:20px; margin-bottom:20px;">Processing payout.</div>
           <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
           <br /> <br />
           <strong>
                Do not close or refresh this browser. 
                <br />
                Please wait...
            </strong>
            <br />
        </div>
    	<p id="msgBoxid">Do you want to continue?</p>
    </div>
    <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary" onclick="processPayout()">Process Current Payout</button>
    </div>
    </div>
<script type="text/javascript">
	$(document).ready(function() {
		
	});
	
	function processPayout(){
		$(".modal-footer").css("display","none");
		$("#msgBoxid").css("display","none");
		$("#msgLoader").css("display","");
		
		
		$.ajax({
			url : "<?=base_url()?>index.php/admin/payout/processCurrentPayout",
			type: "POST",
			data : {"id":"<?php echo $this->session->userdata('id'); ?>", "type":"encash"},
			success:function(res, textStatus, jqXHR) 	{
				//alert(res);
				var arObj = JSON.parse(res);
				if(parseInt(arObj.error) == 1){
					alert(arObj.msg);
					window.location="<?=base_url()?>index.php/admin/payout";
				} else {
					alert('Payout has been processed.');
					//$( "#myModal" ).dialog( "close" );
					window.location="<?=base_url()?>index.php/admin/payout";
				}
			},
			error: function(jqXHR, textStatus, errorThrown)	{
				alert(errorThrown);   
			}
		});
		
	}
	
	function callModal(){
		$('#myModal').modal({
            show: true,
            keyboard: false,
            backdrop: 'static'
        });
	}
	
	
</script>