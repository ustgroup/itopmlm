<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
	.amt{
		text-align:right !important;
	}
	.drow{
		border-top:1px solid #aaaaaa;
		border-bottom:1px solid #aaaaaa;
	}
		.drow{
		border-left:1px solid #aaaaaa;
		border-right:1px solid #aaaaaa;
	}
</style>

<?php $postData = $this->input->post(); ?>
<div style="width:100%;padding-top:5px">
	
<div class="span4">
	<button name="save"  class="btn btn-warning" onclick="showForm()">&nbsp;Add Stock&nbsp;</button>
</div>	
	
	<div class="span8" style="text-align:right">
	<form class="form-signin" action="<?php echo base_url() . 'index.php/admin/stock' ;?>" method="post" >
	
	<table style="width:100%;">
	<tr>
		<td valign="middle" style="text-align:right">
		Add Stock Date From : <input style="width:100px;height:25px" type="text" class="dt" id="datefr" name="datefr" placeholder="From" value="<?php echo ( empty($postData['datefr']) ? '' : $postData['datefr']); ?>"/>
		To : <input style="width:100px;height:25px" type="text" class="dt" id="dateto" name="dateto" placeholder="To" value="<?php echo (empty($postData['dateto']) ? '': $postData['dateto']); ?>"/>
		</td>
		<td valign="top" style="text-align:right;width:100px" >
		<input type="button" class="btn btn-warning" value="Apply Filter" onclick ="this.form.submit()"/>
		</td>
	</tr>
	</table>
	</form>
	</div>
</div>
<br clear="all"><br clear="all">

<div style="width:100%;">

	
    <table id="tblLists" class="table table-striped table-hover table-bordered" >
        <thead>
            <tr style="background:#FDFDFD;">				                
				<th>Date</th>
				<th>Total Amount</th>                				
				<th>Ref#</th>
            </tr>
        </thead>
        
    </table>
</div>
 <div id="prodModal" title="Add Stock" style="display:none" >

						<table>
							<tr>
							<td>*Date</td>
							<td>:<input type="text"  id="datec"  />	</td>
							</tr>							
							<tr>
							<td>*Ref# </td>
							<td>:<input type="text" id="refno" value="<?php echo time(); ?>" readonly="readonly" ></td>
							</tr>							
							<!--tr>
							<td>*Member </td>							
							<td>:<select id="member" name="member" style="width:300px"></select></td>
							</tr-->							
						</table>
						<br clear="all">
						<table style="width:100%">
							<tr style="background:#71A06E;color:#fff">
									<td class="co">Product Name</td>									
									<td class="hp">Product <br>Cost</td>								
									<td class="hs">Qty</td>
									<td class="hs">Amount</td>
									<td>&nbsp;</td>
							</tr>
						
							<tr id="row0" >
								<td id="prodtd" class="co" style="width:400px">
									<select onchange="plist(this.value,0)" style="width:100%" id="prod0"  name="prod[]"    class="prod">				
											<option value="">Select Product Name</option>
																
									</select>
								</td>
							
							
								<td class="hp">
									<input style="width:100px;text-align:right"  type="text" class="inpbox qty" id="price0" name="price[]"   onkeyup="comamt(0)"  />
									
								</td>
								
							
								<td class="hs">
									<input style="width:25px"  type="text" class="inpbox qty" id="qty0" name="qty[]" placeholder="Qty" onkeyup="comamt(0)" />
								</td>
								<td class="hp">
									<input style="width:100px;text-align:right"  type="text" class="inpbox qty" id="amt0" name="amt[]"  readonly="readonly"    />
								</td>
								<td valign="top" ><input type="button" value="&nbsp;x&nbsp;" class="btn"   disabled="disabled"></td>
							</tr>
						</table>
						
						<table name="otherprod" id="otherprod" style="width:100%">
							
						</table>
						<hr>
						<table style="width:100%;border:2px solid #fff">
						
							<tr>
							<td class="co" style=";border:2px solid #fff">			
								<input type="button" name="add" id="add" value="Add Item"  style="width:90px"/>				
							</td>
							
							<td class="hp" style=";border:2px solid #fff;text-align:right" colspan="4"><strong>Total : </strong><input style="width:100px;text-align:right;font-weight:bold"  type="text" class="inpbox" id="tamt" name="tamt"  readonly="readonly"/></td>
							<td style="width:140px">&nbsp;</td>							
							
							</tr>
						</table>
						
						
					   <div style="text-align:center; display:none" id="prodLoader">								  
								<br />    
								<br />    
							   <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
							   <br />                      <strong>                            
									Saving...
								</strong>
						</div>
           
  </div>

  
  
  <div id="details" title="Add Stock Details" style=" display:none" >                   
	<div class="modal-body" id="detailsDiv">						                    	
	</div>                    
  </div>
  
  <?php //$item = json_encode($data); ?>

 <script type="text/javascript">
 var allprod = new Array();	
 ///////////////
 var _jsprodinfo =  new Array();
 var jsprodinfo = new Array();	
 
 $(document).ready(function(e) {
 	$.ajax({
					url : "<?=base_url()?>index.php/admin/stock/productsLists",
					type: "POST",
					
					success:function(res, textStatus, jqXHR) 	{
						 _jsprodinfo =  JSON.parse(res);						 
						for(cnt1 in _jsprodinfo)
						 {    							
							cnt2 = _jsprodinfo[cnt1].id;						
							jsprodinfo[cnt2]= new Object();		
							jsprodinfo[cnt2].id        = _jsprodinfo[cnt1].id;		
							jsprodinfo[cnt2].pname      = _jsprodinfo[cnt1].pname;								
							jsprodinfo[cnt2].pprice      = _jsprodinfo[cnt1].pcost;														
						}						
					//alert(_jsprodinfo);
				
					},
					error: function(jqXHR, textStatus, errorThrown)	{
						alert(errorThrown);   
					}
				});
			
	   });

 ///////////////
  
function plist(id,r)
{		

	if(id >0){
		/*
		if(jsprodinfo[id].stock-jsprodinfo[id].sales >0){
			$("#qty" + r).removeAttr('disabled');		
		}else{
			alert('Empty Stock');
			$("#qty" + r).attr('disabled','disabled');	
		}
		*/
		
		$("#price" + r).val(jsprodinfo[id].pprice);		
		
		$("#qty" + r).val('');
		$("#amt" + r).val('');		
		$("#qty" + r).focus();			
	}else{
		$("#pv" + r).val('');						
		$("#price" + r).val('');		
		$("#amt" + r).val('');
		$("#qty" + r).val('');
	}		
}		

var ctr = 0;
$("#add").click(function()
	{		

	
		ctr = ctr + 1;
		strhtml = '';
		strhtml = strhtml + '<tr id="row' + ctr + '">';			
		strhtml = strhtml + '<td class="co" style="width:400px" >';
		strhtml = strhtml + '<select onchange="plist(this.value,'+ctr+')" style="width:380px" id="prod'+ctr+'"  name="prod[]"    class="prod">';
		strhtml = strhtml + allprod;									
		strhtml = strhtml + '</select></td>';		
		
		strhtml = strhtml + '<td class="hp"><input style="width:100px;text-align:right"  type="text" class="inpbox qty" id="price'+ctr+'" name="price[]"  onkeyup="comamt('+ctr+')" />';		
		strhtml = strhtml + '</td>';			
		strhtml = strhtml + '<td class="hs"><input style="width:25px"  type="text" class="inpbox qty" id="qty'+ctr+'" name="qty[]" placeholder="Qty" onkeyup="comamt('+ctr+')" /></td>';					
		strhtml = strhtml + '<td class="hp"><input style="width:100px;text-align:right"  type="text" class="inpbox" id="amt'+ctr+'" name="amt[]"  readonly="readonly"/></td>';
		
		strhtml = strhtml + '<td valign="top"><input type="button" value="&nbsp;x&nbsp;" class="btn"  onClick="deleteItem(' + ctr + ')"></td>';
		strhtml = strhtml + '</tr>';						
		$("#otherprod").append(strhtml);	
		//$("#divamt").append('<br><input style="width:50px;text-align:right"  type="text" class="inpbox" id="amt'+ctr+'" name="amt[]"   /><input style="width:50px;text-align:right"  type="text" class="inpbox" id="amtd'+ctr+'" name="amtd[]"  readonly="readonly" /><input style="width:50px;text-align:right"  type="text" class="inpbox" id="amtv'+ctr+'" name="amtv[]"  readonly="readonly" />');	
		jQuery('select').chosen({
					disable_search_threshold : 10,
					allow_single_deselect : true
				});		
		$(".qty").numeric();
	});		
	
	
		
function deleteItem(counter)
{	
	
	$("#row" + counter).remove();	
	okcomamt();		
	
}

function formatCurrency(strValue)	{	strValue = strValue.toString().replace(/\$|\,/g,'');	dblValue = parseFloat(strValue); blnSign = (dblValue == (dblValue = Math.abs(dblValue)));	dblValue = Math.floor(dblValue*100+0.50000000001);	intCents = dblValue%100; strCents = intCents.toString();	dblValue = Math.floor(dblValue/100).toString();	if(intCents<10)	strCents = "0" + strCents;	for (var i = 0; i < Math.floor((dblValue.length-(1+i))/3); i++)	dblValue = dblValue.substring(0,dblValue.length-(4*i+3))+','+ dblValue.substring(dblValue.length-(4*i+3));	return (((blnSign)?'':'-') + dblValue + '.' + strCents);	}

function comamt(r)
{				
	var amt = $("#price" + r).val() * $("#qty" + r).val();	
	$("#amt" + r).val('');
	$("#amt" + r).val(amt);	
	
	okcomamt();				
	
}

function okcomamt()
{					
		var tamt = $('input[name="amt[]"]').map(function(){				
		   return this.value
		}).get()		
		var result=tamt.toString().split(",");					
		var total = 0;
		for (var i = 0; i < result.length; i++) {					
			if(parseFloat(result[i])){
				total = total + parseFloat(result[i]);
			}
		}		
		$("#tamt").val(formatCurrency(total));	
}		
</script>

<script type="text/javascript">


	$(document).ready(function(e) {
				
				$.ajax({
					url : "<?=base_url()?>index.php/admin/stock/getmember",
					type: "POST",
					
					success:function(res, textStatus, jqXHR) 	{
						
						var arObj = JSON.parse(res);
						if(parseInt(arObj.error) == 1){								
								//$( "#member" ).html(arObj.member);	
								$( "#prod0" ).html(arObj.product);	
								allprod = arObj.product ;
								$( 'select' ).chosen({	disable_search_threshold : 10,allow_single_deselect : true,width:"95%"});	
						} 
					},
					error: function(jqXHR, textStatus, errorThrown)	{
						alert(errorThrown);   
					}
				});
								
				$( "#prodModal" ).dialog({
					resizable: true,
					height:600,
					width:950,
					modal: true,
					autoOpen: false,
					closeOnEscape: false,
					buttons: {
					"Save" : function() {
								var prod = $('.prod option:checked').map(function(){					
								   return this.value
								}).get()
														
								var price = $('input[name="price[]"]').map(function(){				
								   return this.value
								}).get()
								
								var qty = $('input[name="qty[]"]').map(function(){				
								   return this.value
								}).get()
								

							
							$("#prodLoader").css("display","");
							
							$.ajax({
								url : "<?=base_url()?>index.php/admin/stock/save",
								type: "POST",
								data : {"datec":$( "#datec" ).val() , "refno":$( "#refno" ).val(), "prod": prod,  "price": price, "qty": qty },
								success:function(res, textStatus, jqXHR) 	{
									//alert(res);
									var arObj = JSON.parse(res);
									$("#prodLoader").css("display","none");
									if(parseInt(arObj.error) == 1){
										alert(arObj.msg);										
										//window.location="<?=base_url()?>index.php/ewallet";
									} else {
										alert(arObj.msg);																				
										//$( "#prodModal" ).dialog("close");
										//codes();
										window.location="<?=base_url()?>index.php/admin/stock";
									}
									
								},
								error: function(jqXHR, textStatus, errorThrown)	{
									alert(errorThrown);   
								}
							});
							
					},
					"Close" : function() {
					$( this ).dialog( "close" );
					}
					}
				});
				
				
				$( "#details" ).dialog({
					resizable: true,
					height:350,
					width:800,
					modal: true,
					autoOpen: false,
					closeOnEscape: false,
					buttons: {
					"Close" : function() {
					$( this ).dialog( "close" );
					}
					}
				});
			
			$( "#datec" ).datepicker({
				  changeMonth: true,
				  changeYear: true,
				  yearRange: '2015:<?php echo date('Y')?>',
				  dateFormat: 'yy-mm-dd' 

				});				
				
	
				
				
        });
		
function showForm(){
	/*
	$( "#pname" ).val('');
	$( "#pprice" ).val('');
	$( "#ppv" ).val('');
	$( "#pid" ).val('0');	
	*/
	$( "#prodModal" ).dialog("open");
	
	
}
	
function codes(){
		var table = $('#tblLists').dataTable( {
			"ajax": "<?=base_url()?>index.php/admin/stock/stockLists/?datefr=<?php echo ( empty($postData['datefr']) ? '' : $postData['datefr']); ?>&dateto=<?php echo ( empty($postData['dateto']) ? '' : $postData['dateto']); ?>",			
			 scrollY: "850px",
			 scrollX: true,
			 scrollCollapse: true,
			 "bDestroy":true,
			"columns": [								
				
				{ "data": "dates"},
				{ "class":"amt","data": "tamt" },				
				{ "data": "refno"}
				
        	]			
		} );
		/*
		
		
		
		new $.fn.dataTable.FixedColumns( table, {
        	leftColumns: 2
    	} );
		*/
}

function viewdetails(id){
	
	$.ajax({
		url : "<?=base_url()?>index.php/admin/stock/details",
		type: "POST",
		data : {"refno": id },
		
		success:function(res, textStatus, jqXHR) 	{
			//alert(res);
			var arObj = JSON.parse(res);
			if(parseInt(arObj.error) == 1){
					
					$( "#spanefno" ).html(id);
					$( "#detailsDiv" ).html(arObj.details);
					$( "#details" ).dialog("open");
				
			} 
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			alert(errorThrown);   
		}
	});



}

$(document).ready(function() {	
		codes();
		$(".qty").numeric();
		
$( "#datefr" ).datepicker({
      changeMonth: true,
      changeYear: true,	  
	  dateFormat: 'yy-mm-dd' 

    });	

	
	
$( "#dateto" ).datepicker({
      changeMonth: true,
      changeYear: true,	  
	  dateFormat: 'yy-mm-dd' 

    });	

				
	} );
</script>