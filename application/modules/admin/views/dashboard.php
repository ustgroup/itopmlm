<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
</style>

<table class="table table-striped table-hover table-bordered">
	
    <tbody>
    	 <tr>
        	<td style="width:15%"><strong>Total Free Slot</strong></td>
			<td ><?php echo $data->freeslot;?></td>
        </tr>
		<tr>
        	<td style="width:15%"><strong>Total Paid Slot</strong></td>
			<td ><?php echo $data->paidslot;?></td>
        </tr>
		
		<tr>
        	<td ><strong>Total Member</strong></td>
			<td ><?php echo ($data->paidslot+$data->freeslot);?></td>
        </tr>
		
		<tr>
        	<td ><strong>Total Unilevel</strong></td>
			<td ><?php echo number_format($data->total_comu,2); ?></td>
        </tr>
		
		<tr>
        	<td ><strong>Total Leadership</strong></td>
			<td ><?php echo  number_format($data->total_coml,2);  ?></td>
        </tr>
		<tr>
        	<td ><strong>Total Room Income</strong></td>
			<td ><?php echo  number_format($data->total_comr,2);  ?></td>
        </tr>
			<!--tr>
        	<td style="width:26%"><strong>Total Members Available Ewallet Balance</strong></td>
			<td ><?php echo  number_format($data->total_com-$data->total_pay,2);?></td>
			</tr-->
    </tbody>
</table>