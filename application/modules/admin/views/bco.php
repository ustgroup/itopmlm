<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
	.amt{
		text-align:right !important;
	}
	.stk{
		text-align:center !important;
	}
</style>

<div style="width:100%;">
	<button name="save"  class="btn btn-warning" onclick="showForm()">&nbsp;Add BCO&nbsp;</button>
	<br clear="all"><br clear="all">
    <table id="tblLists" class="table table-striped table-hover table-bordered" >
        <thead>
            <tr style="background:#FDFDFD;">
				<th style="width:15px">Id#</th>
                <th>Username</th>
                <th>Password</th>
				<th>Address</th>
				
            </tr>
        </thead>
        
    </table>
</div>
 <div id="prodModal" title="BCO" style=" display:none" >
                   
                    <div class="modal-body" id="xfrmDiv">
						<table>
							
							
							<tr>
							<td>Username</td>
							<td>*:<input type="text" id="username" value=""  style="width:250px"  ></td>
							</tr>
							
							<tr>
							<td>Password </td>
							<td>*:<input type="text" id="password" value=""  style="width:250px"  ></td>
							</tr>
							<tr>
							<td>Address</td>
							<td>*:<input type="text" id="address" value=""  style="width:450px"  ></td>
							</tr>
						
							
						</table>
						   <div style="text-align:center; display:none" id="prodLoader">
                      
                       <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
                       <br />                      <strong>                            
                            Saving...
                        </strong>
                    </div>
					
						<input type="hidden" id="pid" value="0" >
                    	
                    </div>                    
  </div>

<script type="text/javascript">

	$(document).ready(function(e) {
				
				$( "#prodModal" ).dialog({
					resizable: true,
					height:380,
					width:600,
					modal: true,
					autoOpen: false,
					closeOnEscape: false,
					buttons: {
					"Save" : function() {
						
						
							$("#prodLoader").css("display","");
							
							$.ajax({
								url : "<?=base_url()?>index.php/admin/bco/save",
								type: "POST",
								data : {"pid":$( "#pid" ).val() , "username":$( "#username" ).val(), "password":$( "#password" ).val(), "address":$( "#address" ).val() },
								success:function(res, textStatus, jqXHR) 	{
									//alert(res);
									var arObj = JSON.parse(res);
									if(parseInt(arObj.error) == 1){
										alert(arObj.msg);
										//window.location="<?=base_url()?>index.php/ewallet";
									} else {
										alert(arObj.msg);																				
										$( "#prodModal" ).dialog("close");
										codes();
										//window.location="<?=base_url()?>index.php/ewallet";
									}
									$("#prodLoader").css("display","none");
								},
								error: function(jqXHR, textStatus, errorThrown)	{
									alert(errorThrown);   
								}
							});
							
					},
					"Close" : function() {
					$( this ).dialog( "close" );
					}
					}
				});
					
            });
function showForm(){
	$( "#pname" ).val('');
	$( "#pcost" ).val('');
	$( "#pprice" ).val('');
	$( "#bprice" ).val('');
	$( "#srp" ).val('');
	$( "#ppv" ).val('');
	$( "#pid" ).val('0');	
	$( "#prodModal" ).dialog("open");
	
	
}

function codes(){
		var table = $('#tblLists').dataTable( {
			"ajax": "<?=base_url()?>index.php/admin/bco/Lists",
			 scrollY: "850px",
			 scrollX: true,
			 scrollCollapse: true,
			 "bDestroy":true,
			"columns": [
				{ "data": "id" },
				{ "data": "username"},														
				{ "data": "password" },
				{ "data": "address"}	
				
        	]			
		} );
		/*
		new $.fn.dataTable.FixedColumns( table, {
        	leftColumns: 2
    	} );
		*/
}

function edit(id){
	
	$.ajax({
		url : "<?=base_url()?>index.php/admin/bco/edit",
		type: "POST",
		data : {"pid": id },
		success:function(res, textStatus, jqXHR) 	{
			//alert(res);
			var arObj = JSON.parse(res);
			if(parseInt(arObj.error) == 1){
				$( "#username" ).val(arObj.username);
				$( "#password" ).val(arObj.password);
				$( "#address" ).val(arObj.address);						
				$( "#pid" ).val(id);		
				$( "#prodModal" ).dialog("open");
				
			} 
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			alert(errorThrown);   
		}
	});



}

$(document).ready(function() {	
		codes();
	} );
</script>