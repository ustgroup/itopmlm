<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
	.amt{
		text-align:right !important;
	}
	.stk{
		text-align:center !important;
	}
</style>

<div style="width:100%;">
	<button name="save"  class="btn btn-warning" onclick="showForm()">&nbsp;Add Product&nbsp;</button>
	<br clear="all"><br clear="all">
    <table id="tblLists" class="table table-striped table-hover table-bordered" >
        <thead>
            <tr style="background:#FDFDFD;">
				<th style="width:15px">#</th>
                <th>Product Name</th>
                <th>Product Cost</th>
				<th>Member Price</th>
				<th>BCO Price</th>
				<th>SRP</th>
				<th>PV</th>
                <th style="width:100px">Current Stock</th>
            </tr>
        </thead>
        
    </table>
</div>
 <div id="prodModal" title="Product" style=" display:none" >
                   
                    <div class="modal-body" id="xfrmDiv">
						<table>
							<tr>
							<td>Product Name </td>
							<td>*:<input type="text" id="pname" value="" style="width:400px" ></td>
							</tr>
							
							<tr>
							<td>Product Cost</td>
							<td>*:<input type="text" id="pcost" value=""  style="width:100px"  ></td>
							</tr>
							
							<tr>
							<td>Member Price </td>
							<td>*:<input type="text" id="pprice" value=""  style="width:100px"  ></td>
							</tr>
							<tr>
							<td>BCO Price </td>
							<td>*:<input type="text" id="bprice" value=""  style="width:100px"  ></td>
							</tr>
							<tr>
							<td>SRP </td>
							<td>*:<input type="text" id="srp" value=""  style="width:100px"  ></td>
							</tr>
							
							
							<tr>
							<td>PV </td>
							<td>*:<input type="text" id="ppv" value="" style="width:100px" ></td>
							</tr>
							
							
						</table>
						   <div style="text-align:center; display:none" id="prodLoader">
                      
                       <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
                       <br />                      <strong>                            
                            Saving...
                        </strong>
                    </div>
					
						<input type="hidden" id="pid" value="0" >
                    	
                    </div>                    
  </div>

<script type="text/javascript">

	$(document).ready(function(e) {
				
				$( "#prodModal" ).dialog({
					resizable: true,
					height:380,
					width:600,
					modal: true,
					autoOpen: false,
					closeOnEscape: false,
					buttons: {
					"Save" : function() {
						
						
							$("#prodLoader").css("display","");
							
							$.ajax({
								url : "<?=base_url()?>index.php/admin/products/save",
								type: "POST",
								data : {"pid":$( "#pid" ).val() , "pname":$( "#pname" ).val(), "pcost":$( "#pcost" ).val(), "pprice":$( "#pprice" ).val(),"bprice":$( "#bprice" ).val(),"srp":$( "#srp" ).val(), "ppv":$( "#ppv" ).val() },
								success:function(res, textStatus, jqXHR) 	{
									//alert(res);
									var arObj = JSON.parse(res);
									if(parseInt(arObj.error) == 1){
										alert(arObj.msg);
										//window.location="<?=base_url()?>index.php/ewallet";
									} else {
										alert(arObj.msg);																				
										$( "#prodModal" ).dialog("close");
										codes();
										//window.location="<?=base_url()?>index.php/ewallet";
									}
									$("#prodLoader").css("display","none");
								},
								error: function(jqXHR, textStatus, errorThrown)	{
									alert(errorThrown);   
								}
							});
							
					},
					"Close" : function() {
					$( this ).dialog( "close" );
					}
					}
				});
					
            });
function showForm(){
	$( "#pname" ).val('');
	$( "#pcost" ).val('');
	$( "#pprice" ).val('');
	$( "#bprice" ).val('');
	$( "#srp" ).val('');
	$( "#ppv" ).val('');
	$( "#pid" ).val('0');	
	$( "#prodModal" ).dialog("open");
	
	
}

function codes(){
		var table = $('#tblLists').dataTable( {
			"ajax": "<?=base_url()?>index.php/admin/products/productsLists",
			 scrollY: "850px",
			 scrollX: true,
			 scrollCollapse: true,
			 "bDestroy":true,
			"columns": [
				{ "data": "id" },
				{ "data": "pname"},				
				{ "class":"amt","data": "pcost" },
				{ "class":"amt","data": "pprice" },
				{ "class":"amt","data": "bprice" },
				{ "class":"amt","data": "srp" },
				{ "class":"amt","data": "ppv" },
				{ "class":"stk","data": "pstock" }
				
        	]			
		} );
		/*
		new $.fn.dataTable.FixedColumns( table, {
        	leftColumns: 2
    	} );
		*/
}

function editprod(id){
	
	$.ajax({
		url : "<?=base_url()?>index.php/admin/products/edit",
		type: "POST",
		data : {"pid": id },
		success:function(res, textStatus, jqXHR) 	{
			//alert(res);
			var arObj = JSON.parse(res);
			if(parseInt(arObj.error) == 1){
				$( "#pname" ).val(arObj.pname);
				$( "#pcost" ).val(arObj.pcost);
				$( "#pprice" ).val(arObj.pprice);
				$( "#bprice" ).val(arObj.bprice);
				$( "#srp" ).val(arObj.srp);
				$( "#ppv" ).val(arObj.ppv);			
				$( "#pid" ).val(id);		
				$( "#prodModal" ).dialog("open");
				
			} 
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			alert(errorThrown);   
		}
	});



}

$(document).ready(function() {	
		codes();
	} );
</script>