<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
	.amt{
		text-align:right !important;
	}
	.stk{
		text-align:center !important;
	}
</style>

<div style="width:100%;">
	
    <table id="tblLists" class="table table-striped table-hover table-bordered" >
        <thead>
            <tr style="background:#FDFDFD;">
				<th style="width:15px">#</th>
                <th>Product Name</th>                
				<th>Member Price</th>
				<th>BCO Price</th>
				<th>SRP</th>
				<th>PV</th>
                <th style="width:100px">Current Stock</th>
            </tr>
        </thead>
        
    </table>
</div>
 

<script type="text/javascript">

function codes(){
		var table = $('#tblLists').dataTable( {
			"ajax": "<?=base_url()?>index.php/admin/bcoproducts/productsLists",
			 scrollY: "850px",
			 scrollX: true,
			 scrollCollapse: true,
			 "bDestroy":true,
			"columns": [
				{ "data": "id" },
				{ "data": "pname"},								
				{ "class":"amt","data": "pprice" },
				{ "class":"amt","data": "bprice" },
				{ "class":"amt","data": "srp" },
				{ "class":"amt","data": "ppv" },
				{ "class":"stk","data": "pstock" }
				
        	]			
		} );
		/*
		new $.fn.dataTable.FixedColumns( table, {
        	leftColumns: 2
    	} );
		*/
}


$(document).ready(function() {	
		codes();
	} );
</script>