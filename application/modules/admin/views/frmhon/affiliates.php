<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
	
</style>
<div style="width:100%;">
    <table id="affiliatesLists" class="table table-striped table-hover table-bordered" >
        <thead>
            <tr style="background:#FDFDFD;">
                <th>Username</th>
                <th>Name</th>
                <th>Account Type</th>
                <th>Payment Type</th>
                <th>Registered Date</th>
                <th>City/Municipality</th>
                <th>Province/State</th>
                <th>Country</th>
            </tr>
        </thead>
        
    </table>
</div>
<?php
/*
<tr>
	<td><?php echo $item->username; ?></td>
	<td><?php echo $item->f_name; ?></td>
	<td><?php echo $item->l_name; ?></td>
	<td><?php echo $item->m_name; ?></td>
	<td><?php echo $item->member_type; ?></td>
	<td><?php echo date("M d, Y",strtotime($item->reg_date)); ?></td>
</tr>
*/
?>
<script type="text/javascript">
	$(document).ready(function() {
		var table = $('#affiliatesLists').dataTable( {
			"ajax": "<?=base_url()?>index.php/admin/affiliates/affiliateList",
			 scrollY: "350px",
			 scrollX: true,
			 scrollCollapse: true,
			"columns": [
				{ "data": "username" },
				{ "data": "fullname" },
				{ "data": "act_type" },
				{ "data": "pay_type" },
				{ "data": "regdate" },
				{ "data": "city" },
				{ "data": "province" },
				{ "data": "country" }
        	]			
		} );
		new $.fn.dataTable.FixedColumns( table, {
        	leftColumns: 2
    	} );
	} );
</script>