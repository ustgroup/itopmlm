<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
	.free{
		color:#FC0707;
	}
</style>

<div style="width:100%;">
	<table>
		
		<tr>
			<td>
				<select name="paidorfree" id="paidorfree" style="width:75px" >										
					<option value="P">Paid</option>
					<option value="F">Free</option>
				</select>
			</td>
			<td><input style="width:30px" type="text" name="qty" placeholder="Qty" id="qty"/></td>
			<td><input style="width:300px" type="text" name="remark" placeholder="Remark" id="remark"/></td>
			<td valign="top"><button name="save"  class="btn btn-warning" onclick="addcodes()">&nbsp;Save&nbsp;</button></td>
			
		</tr>
	</table>
    <table id="regcodesLists" class="table table-striped table-hover table-bordered" >
        <thead>
            <tr style="background:#FDFDFD;">
				<th style="width:15px">#</th>
                <th>Remark</th> 
				<th>Slot</th> 
				<th>Codes</th>      				
                <th>Username</th>
                <th>Name</th>
				<th>Date Encode</th>
            </tr>
        </thead>
        
    </table>
</div>
<div id="dialog" title="Please Wait..." style="display:none">
			<div style="text-align:center;width:100%">  
           <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
			<br /> 			
                Do not close or refresh this browser.                
            </div>
</div>
<script type="text/javascript">
function addcodes(){
		$( "#dialog" ).dialog({ modal: true});	
		 $('button[name="save"]').attr('disabled','disabled');
			
			formdata = new FormData();      	
					if (formdata) {										
							
							formdata.append("qty", $('#qty').val() );							
							formdata.append("paidorfree", $('#paidorfree').val() );	
							formdata.append("remark", $('#remark').val() );	
						
							jQuery.ajax({
							url: "<?=base_url()?>index.php/admin/regcodes/addcodes",
							type: "POST",
							datatype:"json",
							data: formdata,
							processData: false,
							contentType: false,
							success:function(res){
								var result =jQuery.parseJSON(res);	
								$( "#dialog" ).dialog( "close" );		
								if(result.error == 1){	
									alert(result.mes+result.error);									
								}else{
									alert(result.mes);
									//window.location = '<?=base_url()?>index.php/admin/regcodes';	
									codes();	
									$('#qty').val('');
									$('#remark').val('');
								}
								
								$('button[name="save"]').removeAttr('disabled');	
							}
						});						
					}
			
	
	
	}

function codes(){
		var table = $('#regcodesLists').dataTable( {
			"ajax": "<?=base_url()?>index.php/admin/regcodes/regcodesList",
			 scrollY: "1024px",		
			 scrollX: true,	
			 scrollCollapse: true,
			 "aLengthMenu": [100,500,1000],
			 "bDestroy":true,
			"columns": [
				{ "data": "id" },
				{ "data": "remark" },
				{ "data": "paidorfree" },	
				{ "data": "fcode" },					
				{ "data": "username" },
				{ "data": "name" },
				{ "data": "dateencode" }
        	]			
		} );
		/*
		new $.fn.dataTable.FixedColumns( table, {
        	leftColumns: 2
    	} );
		*/
		
}
$(document).ready(function() {	
		codes();
	} );
</script>