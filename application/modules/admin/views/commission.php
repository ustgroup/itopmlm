<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
	.amt{
		text-align:right !important;
	}
</style>

<div style="width:100%;">

    <table id="ewalletLists" class="table table-striped table-hover table-bordered" >
        <thead>
            <tr style="background:#FDFDFD;">
				<th>Username</th>
                <th>Name</th>				
                <th>Total Unilevel</th>
				<th>Unilevel Payout</th>
                <th>Total Leadership</th>
				<th>Leadership Payout</th>
				<th>Total Room Income</th>
				<th>Room Income Payout</th>				
            </tr>
        </thead>
        
    </table>
</div>

<script type="text/javascript">

function codes(){
		var table = $('#ewalletLists').dataTable( {
			"ajax": "<?=base_url()?>index.php/admin/commission/ewalletList",
			 scrollY: "850px",
			 scrollX: true,
			 scrollCollapse: true,
			   "aLengthMenu": [100,500,1000],
			 "bDestroy":true,
			"columns": [
				{ "data":"username"},
				{ "data":"name"},								
				{ "class":"amt","data":"total_comu"},
				{ "class":"amt","data":"total_payu"},
				{ "class":"amt","data":"total_coml"},
				{ "class":"amt","data":"total_payl"},
				{ "class":"amt","data":"total_comr"},
				{ "class":"amt","data":"total_payr"}
        	]			
		} );
		//new $.fn.dataTable.FixedColumns( table, {
        //	leftColumns: 2
    	//} );
}
$(document).ready(function() {	
		codes();
	} );
</script>