<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
	.table-bordered th{
		text-align:center;
	}
	.amt{
		text-align:right !important;
	}
	.free{
		color:#FC0707;
	}
</style>
<button name="delete"  class="btn btn-warning" onclick="xdelete()">&nbsp;Delete&nbsp;</button>
<!--button name="reset"  class="btn btn-warning" onclick="xreset()">&nbsp;Reset&nbsp;</button-->
<div style="width:100%;padding-top:5px">
	
    <table id="memberList" class="table table-striped table-hover table-bordered" >
        <thead>
            <tr style="background:#FDFDFD;">
				<th>Slot</th>
				<th>Username</th>
                <th>Name</th>				
                <th>Date Registered</th>           				
				<th>Sponsor Username</th>
                <th>Sponsor Name</th>
            </tr>
        </thead>
        
    </table>
</div>
<div id="dialog" title="Please Wait..." style="display:none">
			<div style="text-align:center;width:100%">  
           <img src="<?=base_url()?>themes/jc_themes/bootstrap/img/ajax-loader.gif" style="margin: 3px 0px;" />
			<br /> 			
                Do not close or refresh this browser.                
            </div>
</div>
<script type="text/javascript">
function xdelete(){	
	if(confirm("Delete All Members?")){
		$( "#dialog" ).dialog({ modal: true});	
		
		 $('button[name="delete"]').attr('disabled','disabled');		
			formdata = new FormData();      	
					if (formdata) {																	
							formdata.append("usr", '<?php echo $this->session->userdata('group'); ?> ');							
						
							jQuery.ajax({
							url: "<?=base_url()?>index.php/admin/members/delete",
							type: "POST",
							datatype:"json",
							data: formdata,
							processData: false,
							contentType: false,
							success:function(res){
								var result =jQuery.parseJSON(res);															
								$( "#dialog" ).dialog( "close" );	
								if(result.error == 1){	
									alert(result.mes);									
								}else{
									alert(result.mes);
									//window.location = '<?=base_url()?>index.php/admin/regcodes';	
									codes();	
								}
								
								$('button[name="delete"]').removeAttr('disabled');	
							}
						});						
					}
			
	}
	
}

function xreset(){	
	if(confirm("Reset Members? ")){
		$( "#dialog" ).dialog({ modal: true});	
		
		 $('button[name="reset"]').attr('disabled','disabled');		
			formdata = new FormData();      	
					if (formdata) {																	
							formdata.append("usr", '<?php echo $this->session->userdata('group'); ?> ');							
						
							jQuery.ajax({
							url: "<?=base_url()?>index.php/admin/members/reset",
							type: "POST",
							datatype:"json",
							data: formdata,
							processData: false,
							contentType: false,
							success:function(res){
								var result =jQuery.parseJSON(res);															
								$( "#dialog" ).dialog( "close" );	
								if(result.error == 1){	
									alert(result.mes);									
								}else{
									alert(result.mes);
									//window.location = '<?=base_url()?>index.php/admin/regcodes';	
									codes();	
								}
								
								$('button[name="reset"]').removeAttr('disabled');	
							}
						});						
					}
			
	}
	
}


function codes(){
		var table = $('#memberList').dataTable( {
			"ajax": "<?=base_url()?>index.php/admin/members/memberList",
			 scrollY: "1024px",
			 scrollX: true,
			 scrollCollapse: true,
			  "aLengthMenu": [100,500,1000],
			 
			 "bDestroy":true,
			"columns": [
				{ "data":"paidorfree"},
				{ "data":"username"},
				{ "data":"name"},								
				{ "data":"regdate"},
				{ "data":"sponsor"},
				{ "data":"sponsorname"}				
        	]			
		} );
		//new $.fn.dataTable.FixedColumns( table, {
        //	leftColumns: 2
    	//} );
}
$(document).ready(function() {	
		codes();
		
		
	} );
</script>