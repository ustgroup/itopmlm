<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class productsModel extends CI_Model {

		function __construct() {
			parent::__construct();
			$this->load->database();
		}	
		
		public function productsLists(){
			$querys="SELECT p.*,i.stock AS pstock
				FROM rbs_products  AS p 
				LEFT JOIN rbs_products_inventory AS i ON i.pid=p.id AND stockist = 0
				
				
			order by p.pname asc ";			
			$query = $this->db->query($querys);				
			return $query->result() ;
		}
		
		
			public function getprod($pid=0)
		{		
			$result = $this->db->query("SELECT * FROM rbs_products	WHERE `id` ='".$pid."'  ");						
			return $result->row();
		}
			public function chkexist($pid=0,$pname='')
		{
			
			if ($pid >0){
				$result = $this->db->query("SELECT id FROM rbs_products	WHERE `pname` ='".$pname."'  AND `id` !='".$pid."'  ");										
			}else{
				$result = $this->db->query("SELECT id FROM rbs_products	WHERE `pname` ='".$pname."'  ");						
			}
			
			$res = $result->row();
			if (count($res) >0)
			{					
				return true;				
			}else{				
				return false;
			}
			
		}
		
		public function save($data){	
		if($data['pid'] >0){
				$result = $this->db->query(" UPDATE rbs_products SET `pname` = '".$data['pname']."', `pcost` = '".$data['pcost']."' , `pprice` = '".$data['pprice']."'  , `bprice` = '".$data['bprice']."' ,  `srp` = '".$data['srp']."'  , `ppv` = '".$data['ppv']."'   WHERE id = '".$data['pid']."' "); 	
		}else{
				$result = $this->db->query("INSERT INTO rbs_products(pname,pcost,pprice,bprice,srp,ppv) 
										VALUES(	'".$data['pname']."' ,
												'".$data['pcost']."', 																								
												'".$data['pprice']."', 																								
												'".$data['bprice']."',
												'".$data['srp']."',
												'".$data['ppv']."'
												)"); 
												
				$newid = $this->db->insert_id();								
				
				$result = $this->db->query("INSERT INTO rbs_products_inventory(pid) 
										VALUES(	'".$newid."' 
												)"); 
				
		}										
		return true;
	}
	
		
	
}