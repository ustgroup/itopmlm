<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminModel extends CI_Model {

		function __construct() {
			
			parent::__construct();
			$this->load->database();
		}
		
		public function count_member()
		{		
			/*
			$querys="SELECT COUNT(userid) tot  FROM rbs_muster_table 
					 WHERE fslot = '1' AND paidorfree = 'P' ";
			$query = $this->db->query($querys);	
			$e = $query->row();
			
			$querys="SELECT COUNT(userid) tot FROM rbs_muster_table 
					 WHERE fslot = '2'  AND paidorfree = 'P' ";
			$query = $this->db->query($querys);	
			$b = $query->row();
			
			$querys="SELECT COUNT(userid) tot FROM rbs_muster_table 
					 WHERE fslot = '3'  AND paidorfree = 'P' ";
			$query = $this->db->query($querys);	
			$p = $query->row();
		
		
			$querys="SELECT COUNT(userid) tot  FROM rbs_muster_table 
					 WHERE fslot = '1' AND paidorfree = 'F' ";
			$query = $this->db->query($querys);	
			$ef = $query->row();
			
			$querys="SELECT COUNT(userid) tot FROM rbs_muster_table 
					 WHERE fslot = '2'  AND paidorfree = 'F' ";
			$query = $this->db->query($querys);	
			$bf = $query->row();
			
			$querys="SELECT COUNT(userid) tot FROM rbs_muster_table 
					 WHERE fslot = '3'  AND paidorfree = 'F' ";
			$query = $this->db->query($querys);	
			$pf = $query->row();
			*/
			
			$querys="SELECT COUNT(userid) tot  FROM rbs_muster_table 
					 WHERE  paidorfree = 'P' ";
			$query = $this->db->query($querys);	
			$p = $query->row();
			
			$querys="SELECT COUNT(userid) tot  FROM rbs_muster_table 
					 WHERE  paidorfree = 'F' ";
			$query = $this->db->query($querys);	
			$f = $query->row();
		
			$querys="SELECT sum(total_coml) total_coml, sum(total_comr) total_comr , sum(total_comu) total_comu 
					 FROM rbs_muster_table 
					 ";
			$query = $this->db->query($querys);	
			$com = $query->row();
			
			$querys="SELECT sum(total_pay) tot FROM rbs_muster_table 
					 WHERE total_pay > '0' ";
			$query = $this->db->query($querys);	
			$pay = $query->row();
		
			$arrayData = array("paidslot" => $p->tot , "freeslot" => $f->tot , "total_coml" => $com->total_coml ,  "total_comr" => $com->total_comr , "total_comu" => $com->total_comu); 
			// print_r($arrayData);
			return (object) $arrayData;	
			
		}	
			
		public function check_exists($userid, $password)
		{
			// md5($password)
			$query = $this->db->query("SELECT * FROM rbs_users WHERE username='".$userid."' AND password='".$password."' ");
			if ($query->num_rows() > 0)	{
				$row = $query->row();
				$login = array(
							'id'=>$row->id, 
							'username'=>$row->username, 
							'name'=>$row->f_name.' '.$row->l_name, 
							'usertype'=> (($row->user_group == 1)? 'A' : 'M'), 
							'userlevel'=>'1', 
							'email'=>$row->email
						);
				//print_r($login);
				//$login = array('id'=>100, 'name'=>'administrator', 'usertype'=>'M', 'userlevel'=>'1', 'email'=>'admin@localhost.com');
				//print_r($login); die;
				$login = (object) $login;
				$this->set_session($login);
				
				return 1;
			} 
			return 2;
			//die( 'ddd' );
			/*
			$this->db->select(" id, username, ");
			$this->db->select("CONCAT(f_name,' ', l_name) AS name ", FALSE);
			$this->db->from	("tbl_mlm_admin AS a");
			$this->db->where("a.IDNO ", $userid );
			$this->db->where("a.PASSWORD", md5($password) );			
			$login = $this->db->get()->result();	
			
			if ( is_array($login) && count($login) == 1 ) {
				//$this->db->select();
				$result = $this->db->query("SELECT user_id FROM tbl_mlm_sessions WHERE user_id='".$login[0]->ADMIN_ID_PK."' ");
				if ($result->num_rows() > 0) {
					return 3;
				} else {
					// Call set_session to set the user's session vars via CodeIgniter
					$this->set_session($login[0]);
					return 1;
				}
			}else{
				
				return 2;
			}
			
			if($userid == 'admin' && $password == '123456'){
				$login = array('id'=>100, 'name'=>'administrator', 'usertype'=>'M', 'userlevel'=>'1', 'email'=>'admin@localhost.com');
				$login = (object) $login;
				$this->set_session($login);
				return 1;
			} else {
				return 2;
			}
			*/
		}
		
		function set_session($item = array()) {
			$this->session->set_userdata( array(
					'id'=>$item->id,
					'username'=>$item->username, 
					'name'=>$item->name,
					'email'=>$item->email,
					'group'=>$item->usertype,
					'level'=>$item->userlevel,
					'isLoggedIn'=>true
				)
			);
			
			//return $this->set_session_user_id($this->session->userdata('session_id'), array('user_id' =>$item->ADMIN_ID_PK ));
			
		}
		
		function set_session_user_id($sessId='',$data=array()){
			$this->db->where('session_id', $sessId);
			return $this->db->update('tbl_mlm_sessions', $data); 
		}
		
		
		public function autoSessionLogout(){
			$timeout = $this->config->item('sess_expiration') + 600;
			$data = array(
			   'LOGIN' => 0
			);
			
			$current_time =  time() - $timeout;
			//echo time().' ==== '.$current_time;
			return $this->db->delete('tbl_mlm_sessions', array('last_activity < ' => $current_time));
			
		}
		
		public function logMeOut(){
			return $this->db->delete('tbl_mlm_sessions', array('user_id' => $this->session->userdata('id')));
			
		}

	
}