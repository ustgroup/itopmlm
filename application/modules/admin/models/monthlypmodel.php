<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class monthlypModel extends CI_Model {

		function __construct() {
			parent::__construct();
			$this->load->database();
		}	
		
		public function monthyList(){
			$querys="SELECT m.*
			,u.username			
			,CONCAT(u.fname,' ', u.lname) AS name
			
			FROM rbs_monthly_proc  AS m 			
			LEFT JOIN rbs_muster_table AS u ON u.userid=m.userid
			ORDER BY m.id asc ";
			
			$query = $this->db->query($querys);	
			
			return $query->result() ;
		}
		
		public function process_pairing($postData){
							
			$result = $this->db->query(" SELECT s.userid,sum(s.qty*s.ppv) AS tpv 
					FROM `rbs_products_sale` As s
					WHERE month(s.`dates`) = '".$postData['month']."' AND year(s.`dates`) = '".$postData['year']."' ANd s.`status` = '0' GROUP BY s.userid  ");												
			$res = $result->result();
			
			if (count($res) >0){	
					//HAVING tpv >=50 4 maintenance
					foreach ($res AS $itm){
						if($itm->tpv >=50){
							//HAVING tpv >=50
							$result = $this->db->query("INSERT INTO `rbs_monthly_proc` (userid,ownpv,pmonth,pyear) 	VALUES(	'".$itm->userid."','".$itm->tpv."','".$postData['month']."','".$postData['year']."' )"); 	
						}
					}
					
					//UPDATE LEFT OR RIGHT POINTS
					foreach ($res AS $itm){					
							
							//UPDATE LEFT POINTS
							$result = $this->db->query(" UPDATE rbs_muster_table SET `lpointsr` = `lpointsr` + ".$itm->tpv." 
															WHERE userid  IN (SELECT topid FROM `rbs_binarylevel` WHERE userid = '".$itm->userid."' AND position = 'L' ) 
															AND userid  IN (SELECT userid FROM `rbs_monthly_proc` WHERE pmonth = '".$postData['month']."' AND pyear = '".$postData['year']."' ) 
															"); 	

							$result = $this->db->query(" UPDATE rbs_monthly_proc SET `lpointsr` = `lpointsr` + ".$itm->tpv." 
															WHERE userid  IN (SELECT topid FROM `rbs_binarylevel` WHERE userid = '".$itm->userid."' AND position = 'L' ) 
															AND  pmonth = '".$postData['month']."' AND pyear = '".$postData['year']."'  
															"); 
															
							//UPDATE RIGHT POINTS
							$result = $this->db->query(" UPDATE rbs_muster_table SET `rpointsr` = `rpointsr` + ".$itm->tpv." 
															WHERE userid  IN (SELECT topid FROM `rbs_binarylevel` WHERE userid = '".$itm->userid."' AND position = 'R' ) 
															AND userid  IN (SELECT userid FROM `rbs_monthly_proc` WHERE pmonth = '".$postData['month']."' AND pyear = '".$postData['year']."' ) 
															"); 

							$result = $this->db->query(" UPDATE rbs_monthly_proc SET `rpointsr` = `rpointsr` + ".$itm->tpv." 
															WHERE userid  IN (SELECT topid FROM `rbs_binarylevel` WHERE userid = '".$itm->userid."' AND position = 'R' ) 
															AND  pmonth = '".$postData['month']."' AND pyear = '".$postData['year']."'  
															"); 									
					}
					
					//UPDATE REPEAT COMMISSION
					$cur_date = date('Y-m-d');
				
											
				$result = $this->db->query(" SELECT m.userid,m.lpointsr,m.rpointsr				
						FROM `rbs_muster_table` AS m 							
						WHERE m.`lpointsr` >= 200 AND m.`rpointsr` >= 200 ");										
				$items = $result->result();
							
					if (count($items) >0){		
						$tpair = 0;
						$tpv = 0;
						foreach ($items as $item){
								if($item->lpointsr < $item->rpointsr){
									$tpair=floor($item->lpointsr/200);
								}elseif($item->lpointsr > $item->rpointsr){
									$tpair=floor($item->rpointsr/200);
								}elseif($item->lpointsr == $item->rpointsr){
									$tpair=floor($item->rpointsr/200);
								}
							$tpv = $tpair * 200;
							//UPDATE REPEAT POINTS
							$result = $this->db->query(" UPDATE rbs_muster_table SET `lpointsr` =  `lpointsr` - '".$tpv."', `rpointsr` =  `rpointsr` - '".$tpv."'   WHERE userid = '".$item->userid."' "); 	
							
							//UPDATE COMMISSION
							$result = $this->db->query("INSERT INTO rbs_commision (recipient_id,source_id,amount,date_added,com_type) 	
									VALUES(	'".$item->userid."'
									,'0'
									,'".$tpv."'
									,'".$cur_date."'
									,'R'
									)"); 
							
							$result = $this->db->query(" UPDATE rbs_muster_table SET `total_com` =  `total_com` + '".$tpv."'  WHERE userid = '".$item->userid."' "); 		
							
						}
					}
				
				//UPDATE SALES STATUS  = 1	
				$result = $this->db->query(" UPDATE rbs_products_sale SET  `status` = '1'  WHERE month(`dates`) = '".$postData['month']."' AND year(`dates`) = '".$postData['year']."'  "); 		
			}
			
			
		}
		
	
	
		public function chkexist($month,$year)
		{
						
			$result = $this->db->query("SELECT id FROM rbs_monthly_proc	WHERE `pmonth` ='".$month."' AND `pyear` ='".$year."' ");												
			$res = $result->row();
			if (count($res) >0)
			{					
				return true;				
			}else{				
				return false;
			}
			
		}
		
}