<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class membersModel extends CI_Model {

		function __construct() {
			parent::__construct();
			$this->load->database();
		}	
		
		public function memberList(){
			$querys="SELECT s.slot,m.regdate
			,m.username
			,m.paidorfree
			,CONCAT(m.fname,' ', m.lname) AS name
			,r.username AS sponsor,CONCAT(r.fname,' ', r.lname) AS sponsorname
			,u.password
			FROM rbs_muster_table  AS m 
			LEFT JOIN rbs_slot AS s ON s.id=m.fslot
			LEFT JOIN rbs_users AS u ON u.id=m.userid
			LEFT JOIN rbs_muster_table AS r ON r.userid=m.sponsor
			order by m.lname asc ";
			
			$query = $this->db->query($querys);	
			
			return $query->result() ;
		}
	
		public function deletemem(){		
			$result = $this->db->query(" TRUNCATE TABLE `rbs_binarylevel`  ");
			$result = $this->db->query(" TRUNCATE TABLE `rbs_regcodes`  ");
			$result = $this->db->query(" TRUNCATE TABLE `rbs_commision`  ");			
			$result = $this->db->query(" TRUNCATE TABLE `rbs_encashment`  ");
			$result = $this->db->query(" TRUNCATE TABLE `rbs_pairing_table`  ");
			$result = $this->db->query(" UPDATE `rbs_muster_table` SET `left` = 0,`right` = 0
												,`total_com`=0
												,`total_pay`=0
												,`lpoints` = 0
												,`rpoints` = 0   
												
												,`total_coml` = 0   
												,`total_payl` = 0   
												,`total_comr` = 0   
												,`total_payr` = 0   
												,`total_comu` = 0   
												,`total_payu` = 0   
												
												");
			$result = $this->db->query(" DELETE FROM `rbs_muster_table` WHERE `userid` >2  ");
			$result = $this->db->query(" DELETE FROM `rbs_users` WHERE `id` >3  ");					
			
			$result = $this->db->query(" TRUNCATE TABLE `rbs_table_downline`  ");
			$result = $this->db->query(" TRUNCATE TABLE `rbs_table_exit`  ");
			
			$result = $this->db->query(" DELETE FROM `rbs_table_list` WHERE `id` >1  ");					
			$result = $this->db->query(" UPDATE `rbs_table_list` SET `leftf` = 0,`rightf` = 0 ,`stat` = 0 ");
			return 1 ;
		}
		
		public function resetmem(){		
			/*
			$result = $this->db->query(" TRUNCATE TABLE `rbs_commision`  ");			
			$result = $this->db->query(" TRUNCATE TABLE `rbs_encashment`  ");
			$result = $this->db->query(" TRUNCATE TABLE `rbs_pairing_table`  ");
			$result = $this->db->query(" UPDATE `rbs_muster_table` SET `total_com`=0,`total_pay`=0,`lpoints` = 0,`rpoints` = 0   ");
			*/
			
			return 1 ;
		}
		
}