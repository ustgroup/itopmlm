<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class commissionModel extends CI_Model {

		function __construct() {
			parent::__construct();
			$this->load->database();
		}	
		
		public function ewalletList(){
			$querys="SELECT s.slot,m.*,CONCAT(m.fname,' ', m.lname) AS name FROM rbs_muster_table  AS m 
			LEFT JOIN rbs_slot AS s ON s.id=m.fslot
			
			order by m.lname asc ";
			
			$query = $this->db->query($querys);	
			
			return $query->result() ;
		}
	
}