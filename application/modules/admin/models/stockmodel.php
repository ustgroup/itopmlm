<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class stockModel extends CI_Model {

		function __construct() {
			parent::__construct();
			$this->load->database();
		}	
		
		public function productLists(){
			//,(SELECT  sum(qty)  FROM `rbs_products_stock`  WHERE pid = p.id  ) stock
			//,(SELECT sum(qty)   FROM `rbs_products_sale`   WHERE pid = p.id  ) sales	
		
			$querys="SELECT p.*
					FROM `rbs_products` AS p    						
						  
					ORDER BY  p.pname ASC ";			
			$query = $this->db->query($querys);				
			return $query->result() ;
		}
		
		public function memberLists(){
			$querys="SELECT m.userid,m.username,CONCAT(m.fname,' ', m.lname) AS name 			            
					FROM `rbs_muster_table` AS m            
					ORDER BY m.username ASC ";			
			$query = $this->db->query($querys);				
			return $query->result() ;
		}
		
		public function stockLists(){
			$datefr = $_GET['datefr'];
			$dateto = $_GET['dateto'];
			$_GET['dateto'];
						 	
			if (!empty($datefr) && !empty($dateto)) { 								
				$WR = " AND DATEDIFF(s.dates,'" .$datefr. "')>=0 AND  DATEDIFF('" .$dateto. "',s.dates)>=0 " ;	
			}else{
				$WR = " " ;	
			}
			
			$querys="SELECT sum(s.qty*s.pprice) AS tamt,s.dates,s.refno			            
			FROM `rbs_products_stock` AS s  
			WHERE s.stockist = 0	
			$WR
             GROUP BY s.refno ";			
			$query = $this->db->query($querys);				
			return $query->result() ;
		}
		
		
			public function getdetails($pid=0)
		{		
			$result = $this->db->query("SELECT s.*,p.pname FROM rbs_products_stock AS s
			 LEFT JOIN rbs_products p ON p.id=s.pid
			WHERE s.`refno` ='".$pid."'  ");						
			return $result->result();
		}
		
		
			public function chkrefnoexist($refno)
		{
						
			$result = $this->db->query("SELECT id FROM rbs_products_stock	WHERE `refno` ='".$refno."'  ");												
			$res = $result->row();
			if (count($res) >0)
			{					
				return true;				
			}else{				
				return false;
			}
			
		}
		
		public function save($postData){			
			for($x = 0;$x<= count( $postData['prod'] ) - 1;$x++ ){
				if($postData['prod'][$x] >0 &&$postData['qty'][$x] >0 ){
					$result = $this->db->query("INSERT INTO rbs_products_stock(pid,qty,pprice,dates,refno) 
									VALUES(	'".$postData['prod'][$x]."' ,
											'".$postData['qty'][$x]."', 																																			
											'".$postData['price'][$x]."',											
											'".$postData['datec']."',
											'".$postData['refno']."'											
											)"); 
											
					$result = $this->db->query(" UPDATE rbs_products_inventory SET `stock` =  `stock` + '".$postData['qty'][$x]."'    WHERE pid = '".$postData['prod'][$x]."' AND stockist = '0' "); 								
				}						
			}													
		return true;
	}
	
		
	
}