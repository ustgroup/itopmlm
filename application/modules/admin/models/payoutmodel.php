<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class payoutModel extends CI_Model {

		function __construct() {
			parent::__construct();
			$this->load->database();
		}	
		
		public function getEncashment(){
			$postData = $this->input->post();	
			if(empty( $postData['batch'])){
				$wr = "WHERE p.status = '0' ";
			}else{
				$wr = "WHERE p.process_date = '".$postData['batch']."' ";
			}
			$querys =" SELECT p.*, m.username, CONCAT(m.lname,' ',m.fname,' ',m.mname) fullname " 
					." FROM rbs_encashment AS p "
					." LEFT JOIN rbs_muster_table AS m ON m.userid = p.userid "
					."  ".$wr." ORDER BY p.request_date ASC ";
			//echo $querys;
			$query = $this->db->query($querys);	
			return $query->result() ;
		}
		
		
		public function getBatch(){			
			$querys =" SELECT p.process_date " 
					." FROM rbs_encashment AS p "					
					." WHERE p.status = '1' GROUP BY p.process_date  ORDER BY  p.process_date ASC ";
			//echo $querys;
			$query = $this->db->query($querys);	
			return $query->result() ;
		}
		
		public function processPayout(){
			$updateQry = "UPDATE rbs_encashment SET process_date = '".date("Y-m-d H:i:s")."', process_by='".$this->session->userdata('id')."', status= '1' WHERE status = '0' ";
			if($this->db->query($updateQry)){
				return 0;
			} else {
				return 1;
			}
		}
	
}