<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bigene extends CI_Controller {

	public function __construct() {
		
		parent::__construct();  
		
	
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index() {
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				$this->load->model('bigeneModel','model');
					
				$this->template->add_css('themes/jc_themes/chosen/chosen.css');
				$this->template->add_js('themes/jc_themes/chosen/chosen.jquery.js');
					
				//print_r($data);
				$postData = $this->input->post();	
				
				$getData = $this->input->get();					
				if($postData['downline'] > 0){
					$getData['id'] = $postData['downline'] ;
				}
				if(!empty($getData['id'])){
					if( $this->session->userdata('id') > $getData['id']  ){
						$res['data'] = $this->model->getGene($this->session->userdata('id'));
						$res['direct'] = $this->model->getDirect($this->session->userdata('id'));
					}else{				
						$res['data'] = $this->model->getGene($getData['id']);
						$res['direct'] = $this->model->getDirect($getData['id']);
					}
				}else{
					$res['data'] = $this->model->getGene($this->session->userdata('id'));
					$res['direct'] = $this->model->getDirect($this->session->userdata('id'));
				}
				
				$slot['slot'] = $this->model->getSlot($this->session->userdata('id'));
				//$res['items'] = $this->model->getEncashmentHistory($this->session->userdata('id'));
				$this->template->write_view('sidebar', 'main/sidebar',$slot);
				$this->template->write_view('content', 'bigene',$res);
				
				$this->template->render();
			} else {
				redirect('admin');
			}
		} else {
			redirect('');
		}
	}
	
	public function getDownline() {
		if( $this->session->userdata('isLoggedIn') ) {
			if($this->session->userdata('group')=='M'){
				$postData = $this->input->post();
			$this->load->model('bigeneModel','model');							
			$resc = $this->model->downlineLists($this->session->userdata('id'));
			
			
				$tbl='<option value="0">Select Downline</option>';
				foreach($resc AS $itm){				
					$tbl.='<option value="'.$itm->userid.'">'.$itm->name.'</option>';						
				}
				echo json_encode(array("error"=>1, "downline"=> $tbl ));
			}else {
				redirect('admin');
			}
			
		}else{
			redirect('');
		}
		
	}	
	public function id() {
		die('ddd');
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */