<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bigeneModel extends CI_Model {

		function __construct() {
			
			parent::__construct();
			$this->load->database();
		}	
		var $item;	
		
		public function getSlot($userid=0){
			$querys="SELECT s.slot,m.fslot FROM rbs_muster_table  AS m
						LEFT JOIN rbs_slot AS s ON s.id=m.fslot
					 WHERE m.userid = '".$userid."' ";
			$query = $this->db->query($querys);	
			return $query->row();
			
		}
		public function downlineLists($id){
			$querys="SELECT l.userid, CONCAT(m.username, ' - ' , m.fname , ' ' , m.lname ) AS name		            
					FROM `rbs_binarylevel` AS l
					LEFT JOIN `rbs_muster_table` m ON m.userid=l.userid	
					WHERE l.topid = '".$id."' 
					GROUP BY l.userid 
					ORDER BY m.username ASC
					
					";			
			$query = $this->db->query($querys);				
			return $query->result() ;
		}
		
		public function getGene($id=0){
		
				$result = $this->db->query(" SELECT `left`,`right`,`upline`,`rpoints`,`lpoints`,`rpointsr`,`lpointsr`,`username` FROM `rbs_muster_table` where userid = '".$id."'  ");									
				$this->item['list'] = $result->row();
					
				
				$result = $this->db->query(" SELECT 				              				
							 l.*
							,m.username															
							FROM `rbs_table_list` AS l 
							LEFT JOIN `rbs_muster_table` m ON m.userid=l.userid														
			 
							WHERE l.`userid` = '".$id."' ORDER BY tablef ASC ");										
				$this->item['table'] = $result->result();
				//print_r($this->item['table']);
				
				$this->item['top'] = $id;
				
				$this->item['a'] = $id;				
				$this->item['b'] = $this->item['list']->left;
				$this->item['c'] = $this->item['list']->right;
				$this->item['upline'] = $this->item['list']->upline;
								
				$bvar = $this->getLR($this->item['b']);
				$this->item['d'] = $bvar->left;
				$this->item['e'] = $bvar->right;
				
				$cvar = $this->getLR($this->item['c']);
				$this->item['f'] = $cvar->left;
				$this->item['g'] = $cvar->right;
				
				$dvar = $this->getLR($this->item['d']);
				$this->item['1'] = $dvar->left;
				$this->item['2'] = $dvar->right;
				
				$evar = $this->getLR($this->item['e']);
				$this->item['3'] = $evar->left;
				$this->item['4'] = $evar->right;
								
				$fvar = $this->getLR($this->item['f']);
				$this->item['5'] = $fvar->left;
				$this->item['6'] = $fvar->right;
				
				$gvar = $this->getLR($this->item['g']);
				$this->item['7'] = $gvar->left;
				$this->item['8'] = $gvar->right;
				
				
				
				$result = $this->db->query(" SELECT lpoints,rpoints  FROM `rbs_muster_table` where userid = '".$id."'  ");
				
				$this->item['waitpoints'] = $result->row();
				
			 	return $this->item;	
		}		
		
		
		public function getLR($id = null) 
		{	
				
			if($id){						
				$result = $this->db->query(" SELECT `left`,`right` FROM `rbs_muster_table` where userid = '".$id."'  ");				
				$xitems = $result->row();
		
			}else{
				$xitems = (object) array('left' => '0', 'right' => '0');					
			}								
			return $xitems;						
		}
		
		public function getInfo($id = null) 
		{						
			if($id){					
			
				$result = $this->db->query(" SELECT `fname`,`mname`,`lname`,`username`,`userid`,`fslot`,`paidorfree` FROM `rbs_muster_table` where userid = '".$id."'  ");				
				$xitems = $result->row();	
				
				
				if($xitems->paidorfree=='F'){
					$xitems->img = '<img src = "'.base_url().'themes/jc_themes/phar3x.png" />';				
				}else{
					$xitems->img = '<img src = "'.base_url().'themes/jc_themes/phar3.png" />';
				}
				
			}else{
				$xitems = (object) array('fname' => '', 'mname' => '', 'lname' => '', 'username' => '', 'userid' => '', 'fslot' => '', 'img' => '');					
			}								
			return $xitems;						
		}
	
		public function getDownline($topid = 0,$table = 0,$pos = '') 
		{														
			$result = $this->db->query(" SELECT 				              				
							 d.userid
							,d.tablef
							,m.username															
							FROM `rbs_table_downline` AS d 
							LEFT JOIN `rbs_muster_table` m ON m.userid=d.userid														
			 
							WHERE d.`topid` = '".$topid."' AND  d.`tablef` = '".$table."' AND d.position = '".$pos."'  ORDER BY d.id ASC ");					
			$xitems = $result->result();														
			return $xitems;						
		}
		
			public function countDownline($topid = 0,$table = 0,$pos = '') 
		{														
			$result = $this->db->query(" SELECT COUNT(d.userid)	AS total			              											
							FROM `rbs_table_downline` AS d 										 
							WHERE d.`topid` = '".$topid."' AND  d.`tablef` = '".$table."' AND d.position = '".$pos."' ");					
			$xitems = $result->row();							
			return $xitems->total;						
		}
	
		public function getDirect($id=0){
			$querys=" SELECT s.slot,m.regdate
			,m.username
			,CONCAT(m.fname,' ', m.lname) AS name						
			FROM rbs_muster_table  AS m 
			LEFT JOIN rbs_slot AS s ON s.id=m.fslot
			
			
			where sponsor = '".$id."' order by m.lname asc  ";
			$query = $this->db->query($querys);	
			return $query->result() ;
		}
		
		/////////////////////////////////////////////////////
		

		
}